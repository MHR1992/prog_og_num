
EXERCISE B:

1a,1b.1 and 1b.2 argumentation:
a: (for implementation see code and times.svg - only elimination of 1st row) 
b.1: It would be a problem if each loop used the whole matrix. 
We only consider the upper triangular part and as p increases 1 
for each loop the zeroed elements are not affected. 
b.2: 
2: Change phi to phi+phi/2
3: It can be seen from times.svg that if one only needs the 
lowest/highest eigenvalue then the e-by-e method is probably 
preferable. 
4: However from times_full.svg we see that if one wants full diagonalization then e-by-e method is not good for large N!.

Result of Jacobi e-by-e diagonalization: 
n = 5, sweeps = 125
eigenvalues:
 -0.557 -0.124  0.069  0.461  3.059
Check that V^TAV==D (same as checking diagonal(B^TVB) == e):
 -0.557 -0.000  0.000  0.000 -0.000
 -0.000 -0.124 -0.000 -0.000 -0.000
  0.000 -0.000  0.069 -0.000 -0.000
  0.000 -0.000 -0.000  0.461 -0.000
 -0.000 -0.000 -0.000 -0.000  3.059
We see that the eigenvalues on the diagonal are in ascending order
just as expected.
