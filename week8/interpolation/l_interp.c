double l_interp(int n, double * x, double * y, double z){
	int i = 0, j = n-1;
	while(j-i > 1) {
		int mid = (i+j)/2;
		if(z >= x[mid]) i = mid;
		else j = mid;
	}

	double a_i = y[i];
	double b_i = (y[i+1]-y[i])/(x[i+1]-x[i]);
	return a_i + b_i * (z-x[i]);
}

/* i og j bliver større eller mindre afhængig af om z er større eller lavere
end x[mid].*/

double l_interp_integ(int n, double * x, double * y, double z){
       int i = 0, j = n-1;
        while(j-i > 1) {
                int mid = (i+j)/2;
                if(z >= x[mid]) i = mid;
                else j = mid;
        }
        double integral = y[i]+(l_interp(n,x,y,z)-y[i])/(z-x[i])/2;
	for(int k = 0; k<i; k++){
		integral += y[k]+(y[k+1]-y[k])/(x[k+1]-x[k])/2;
	}
	return integral;
}

