#include"mv_operations.h"
#include"qr_gs.h"

void newton_with_jacobian(void f(const vector * x, vector * fx, matrix * Jacobian, int * ncallsp), vector * x, int * ncallsp, double dx, double eps){

	int n = x->size;
        matrix * Jacobian = matrix_alloc(n, n);
        matrix * R = matrix_alloc(n, n);
        vector * fx = vector_alloc(n);
        vector * z = vector_alloc(n);
        vector * fz = vector_alloc(n);
        vector * Dx = vector_alloc(n);                                  //stepsize vector

	while(1){
		f(x, fx, Jacobian, ncallsp);
		qr_gs_decomp(Jacobian, R);                                 //for solving (5)
		qr_gs_solve(Dx, Jacobian, R, fx);                          //find dx that solves system
		vector_scale(Dx, -1);
		double lambda = 1;
		while(1){
			vector_cpy(z, x);
			vector_add(1, z, 1, Dx);                        // z = x+Dx
			f(z, fz, Jacobian, ncallsp);                                      // fz = f(x+Dx)
			if(vector_modulus(fz) < (1-lambda/2)*vector_modulus(fx) || lambda < 0.02) break;
			lambda *= 0.5;
			vector_scale(Dx, 0.5);                          //Dx include lambdas!!
		}
		vector_cpy(x, z);                                       //overwrite x and fx with new val$
		vector_cpy(fx, fz);
		if(vector_modulus(Dx) < dx || vector_modulus(fx) < eps) break;
        }
	matrix_free(Jacobian);
	matrix_free(R);
	vector_free(fx);
	vector_free(z);
	vector_free(fz);
	vector_free(Dx);



}

// f(v, fv) uses v[0] and v[1] to set fv
void newton(void f(vector * x, vector * fx, int * ncallsp), vector * x, int * ncallsp, double dx, double eps){ //dx: step size
	int n = x->size;
	matrix * Jacob = matrix_alloc(n, n);
	matrix * R = matrix_alloc(n, n);
	vector * fx = vector_alloc(n);
	vector * z = vector_alloc(n);
	vector * fz = vector_alloc(n);
	vector * df = vector_alloc(n);
	vector * Dx = vector_alloc(n);					//stepsize vector

	while(1){
		f(x, fx, ncallsp);						//fx = fx
		for(int j = 0; j < n; j++){
			vector_set(x, j, vector_get(x, j) + dx);	//change x coordinates
			f(x, df, ncallsp);					//df = f(x+dx)
			vector_sub(1, df, 1, fx);			//df = f(x+dx)-f(x)
			for(int i = 0; i < n; i++){			//setting Jacobian matrix
				matrix_set(Jacob, i, j, vector_get(df, i)/dx);
			}
			vector_set(x, j, vector_get(x, j)-dx);		//change x coordinates back
		}
		qr_gs_decomp(Jacob, R);					//for solving (5)
		qr_gs_solve(Dx, Jacob, R, fx);				//find dx that solves system
		vector_scale(Dx, -1);					//there is a "-" in (5)
		double lambda = 1;
		while(1){
			vector_cpy(z, x);
			vector_add(1, z, 1, Dx);			// z = x+Dx
			f(z, fz, ncallsp);					// fz = f(x+Dx)
			if(vector_modulus(fz) < (1-lambda/2)*vector_modulus(fx) || lambda < 1) break;
			lambda *= 0.5;
			vector_scale(Dx, 0.5);				//Dx include lambdas!!
		}
		vector_cpy(x, z);					//overwrite x and fx with new values
		vector_cpy(fx, fz);
		if(vector_modulus(Dx) < dx || vector_modulus(fx) < eps) break;
	}
	matrix_free(Jacob);
	matrix_free(R);
	vector_free(fx);
	vector_free(z);
	vector_free(fz);
	vector_free(df);
	vector_free(Dx);
}


