#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"simple_operations.h"
#define RND (double) rand()/RAND_MAX
#define max_print 10
//Exercise B
int jacobiB(int num_of_eig, gsl_matrix * A, gsl_vector * e, gsl_matrix * V);

int main(int argc, char** argv){
	int n = (argc > 1? atoi(argv[1]):5);
	int num_of_eigv = (argc > 2? atoi(argv[2]):1);
	gsl_matrix * C = gsl_matrix_alloc(n,n);


	for(int r = 0; r < n; r++){
		for(int c = r; c < n; c++){
			double x = RND;
			gsl_matrix_set(C, r, c, x);
			gsl_matrix_set(C, c, r, x);
		}
	}

	gsl_matrix * D = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(D, C);
	gsl_matrix * W = gsl_matrix_alloc(n,n);
	gsl_vector * e2 = gsl_vector_alloc(n);
	int sweeps2 = jacobiB(num_of_eigv, C, e2, W);


	if(n < max_print){
		fprintf(stderr, "\nEXERCISE B:\n\n");
		fprintf(stderr, "1a,1b.1 and 1b.2 argumentation:\n");
		fprintf(stderr, "a: (for implementation see code and times.svg - only elimination of 1st row) \nb.1: It would be a problem if each loop used the whole matrix. \nWe only consider the upper triangular part and as p increases 1 \nfor each loop the zeroed elements are not affected. \nb.2: \n");
		fprintf(stderr, "2: Change phi to phi+phi/2\n3: It can be seen from times.svg that if one only needs the \nlowest/highest eigenvalue then the e-by-e method is probably \npreferable. \n4: However from times_full.svg we see that if one wants full diagonalization then e-by-e method is not good for large N!.\n\n");
		fprintf(stderr, "Result of Jacobi e-by-e diagonalization: \n");
		fprintf(stderr, "n = %i, sweeps = %i\n", n, sweeps2);
		fprintf(stderr, "eigenvalues:\n"); vector_print(e2);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, D, W, 0, C);
		gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, W, C, 0, D);
		fprintf(stderr, "\nCheck that V^TAV==D (same as checking diagonal(B^TVB) == e):\n");
		matrix_print(D);
		fprintf(stderr, "We see that the eigenvalues on the diagonal are in ascending order\njust as expected.\n");
	}
	//free matrices and vectors again!
	gsl_matrix_free(W);
	gsl_vector_free(e2);
	gsl_matrix_free(C);
	gsl_matrix_free(D);
return 0;
}


