#ifndef HAVE_QR
#define HAVE_QR
void qr_gs_decomp  	(gsl_matrix* A, gsl_matrix* R);
void qr_gs_backsub  	(gsl_matrix* A, gsl_vector* R);
void qr_gs_solve	(gsl_vector * x, gsl_matrix* Q, gsl_matrix* R, gsl_vector* b);
void qr_gs_inv  	(gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);
#endif
