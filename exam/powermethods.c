#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include"qr_gs_gsl.h"
#include"operations.h"

double rayleigh_quotient(gsl_matrix * A, gsl_vector * x){
        gsl_vector * Ax = gsl_vector_alloc(x->size);
        double lambda, xTx;
        gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, Ax);
        gsl_blas_ddot(x, Ax, &lambda);
        gsl_blas_ddot(x, x, &xTx);
        lambda = lambda/xTx;
        return lambda;
}

void inverse_power_method(gsl_matrix * A, double s_init, gsl_vector * x_init,
	int rayleigh_switch, double abs, double rel){

	//alloc
	gsl_vector * x_i_new = gsl_vector_calloc(A->size2);
	gsl_matrix * A_s1 = gsl_matrix_alloc(A->size1, A->size2);
	gsl_matrix * s1 = gsl_matrix_calloc(A->size1, A->size2);
	//R is for QR decomposition
	gsl_matrix * R = gsl_matrix_calloc(A->size2, A->size2);

	int i = 0;
	while (1) {
		i++;
		//creating matrix A-sI
		gsl_matrix_memcpy(A_s1, A);
		gsl_matrix_set_identity(s1);
		gsl_matrix_scale(s1, s_init);
		gsl_matrix_sub(A_s1, s1);

		//updating s with Rayleigh quotient for (ideally) faster
		//convergence towards nearest eigenvalue. However, although
		//enabling this Rayleigh update seems to make convergence
		//faster, the convergence to the nearest eigenvalue is also
		//more unsure (makes sense as we change s!).
		if(rayleigh_switch == 1 && i%2 == 0){
			s_init = rayleigh_quotient(A, x_init);
		}

		//QR decomp
		qr_gs_decomp(A_s1, R);
		qr_gs_solve(x_i_new, A_s1, R, x_init);

		//for checking if converged
		gsl_vector * Ax_i_new = gsl_vector_calloc(A->size1);
		gsl_vector * sx_i_new = gsl_vector_calloc(A->size1);
		gsl_blas_dgemv(CblasNoTrans, 1, A, x_i_new, 0, Ax_i_new);
		gsl_blas_daxpy(rayleigh_quotient(A, x_init), x_i_new, sx_i_new);
		gsl_vector_memcpy(x_init, x_i_new);

		if(check_gsl_vectors_equal(Ax_i_new, sx_i_new, abs, rel) == 1){
			printf("\nConverged to eigenvalue: %f after %d iterations. \n", rayleigh_quotient(A, x_i_new), i);
			printf("\nFinal eigenvector with above eigenvalue: \n");
			double x_final_nrm = gsl_blas_dnrm2(x_init);
			gsl_vector_scale(x_init, 1/x_final_nrm);
			gsl_vector_fprintf(stdout, x_init, "%g");
			printf("\n");

			//free
			gsl_vector_free(Ax_i_new);
			gsl_vector_free(sx_i_new);
			break;
		}

		gsl_vector_free(Ax_i_new);
		gsl_vector_free(sx_i_new);

		if(i > 1000){
			printf("\nMax number of iterations reached...\n");
			printf("\nLast approximation to eigenvalue: %f after %d iterations.\n", rayleigh_quotient(A, x_i_new), i);
			break;
		}
	}
	//free
	gsl_vector_free(x_i_new);
	gsl_matrix_free(A_s1);
	gsl_matrix_free(s1);
	gsl_matrix_free(R);
}


/*
void arnoldi(matrix * A, matrix * Q, matrix * H_n){
	//the random q1 is normalized
	vector * q_1 = vector_alloc(Q->size1);
	matrix_get_column(Q, 0, q_1);
	vector_normalize(q1);
	matrix_set_column(Q, 0, q_1);

	for(int k = 1; k < H_n->size1; k++){			//needs to start from 1 as q1 is found!
		vector * q_k_prev = vector_alloc(q1->size);
		vector_cpy(q_k_prev, matrix_get_column(Q, k-1));
		vector * q_k = vector_alloc(q1->size);
		//making new vector q_k = A q_(k-1)
		matrix_vector_dot(q_k, A, q_k_prev);
		//orthogonalization of q_k to all other vectors qi (Gram-Schmidt) and storing of q_i_dagger q_k into H_(i,(k-1))
		for(int i = k-1; i = 1; i--){
			double q_i_q_k = vector_dot(q_i,q_k);
			vector * q_i = vector_alloc(q1->size);
			vector_cpy(q_i, matrix_get_column(Q, i));
			vector_sub(1, q_k, q_i_q_k, q_i);
			matrix_set(H_n, i, k-1, q_i_q_k);
		}
		//normalization of q_k and storing in H_(k, (k-1))
		vector_normalize(q_k);
		double len_q_k = vector_modulus(q_k);
		matrix_set(H_n, k, k-1, len_q_k);
		matrix_set_column(Q, k, q_k);
	}
return 0;
}
*/
