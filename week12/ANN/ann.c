#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"ann.h"
int qnewton(double phi(gsl_vector * x), gsl_vector * x, double eps);
//int amoeba(int d, double F(double *), double * p, double * h, double size_goal);

//allocates space for a neural net
ann * ann_alloc(int n, double(*f)(double)){
	ann * network = malloc(sizeof(ann));
	network->n = n;
	network->f = f;
	network->data = gsl_vector_alloc(3*n);
	return network;
}

//frees space for a neural net
void ann_free(ann * network){
	gsl_vector_free(network->data);
	free(network);
}

//calculates the sum of the outputs of all the hidden layers neurons (s is just the prediction y)
double ann_feed_forward(ann * network, double x){
	double y_nn = 0;
	for(int i = 0; i < network->n; i++){
		double a = gsl_vector_get(network->data, 3*i);
		double b = gsl_vector_get(network->data, 3*i + 1);
		double w = gsl_vector_get(network->data, 3*i + 2);
		y_nn += network->f((x-a)/b)*w;
	}
	return y_nn;
}

double ann_feed_forward_derivative(ann * network, double (* df)(double), double x){
	double dy_nn = 0;
	for(int i = 0; i < network->n; i++){
		double a = gsl_vector_get(network->data, 3*i);
		double b = gsl_vector_get(network->data, 3*i+1);
		double w = gsl_vector_get(network->data, 3*i+2);
		dy_nn += df((x-a)/b)*w;
	}
	return dy_nn;
}

double ann_feed_forward_antiderivative(ann * network, double (* F)(double), double x){
	double Y_nn = 0;
	for(int i = 0; i < network->n; i++){
		double a = gsl_vector_get(network->data, 3*i);
		double b = gsl_vector_get(network->data, 3*i+1);
		double w = gsl_vector_get(network->data, 3*i+2);
		Y_nn += F((x-a)/b)*w;
	}
	return Y_nn;
}


int ann_train(ann * network, gsl_vector * vx, gsl_vector * vy){

	//finds the deviation of the expetation value found by the nn and
	//the actual value y (not the squared deviation!) and sum all the contributions
	//from the different datapoints which are fed to the nn.
	double delta(gsl_vector * p){
		gsl_vector_memcpy(network->data, p);
		double s = 0;
		for(int i = 0; i < vx->size; i++){
			double x_k = gsl_vector_get(vx, i);
			double y_k = gsl_vector_get(vy, i);
			double f_x_k = ann_feed_forward(network, x_k);
			s += fabs(f_x_k-y_k);
		}
		return s/vx->size;
	}

	gsl_vector * p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p, network->data);
	int nsteps = qnewton(delta, p, 1e-3);	//remember that here the minimum is found through these Newton steps! We used this symmetric Broydens update for the Hessian matrix.
	gsl_vector_memcpy(network->data, p);
	gsl_vector_free(p);
return nsteps;
}

/*
int ann_train_with_derivatives(ann * network, double (*df)(double), gsl_vector * vx, gsl_vector * vy, gsl_vector * vdy){

	//finds the deviation of the expetation value found by the nn and
	//the actual value y (not the squared deviation!) and sum all the contributions
	//from the different datapoints which are fed to the nn.
	double delta(gsl_vector * p){
		gsl_vector_memcpy(network->data, p);
		double s = 0;
		for(int i = 0; i < vx->size; i++){
			double x_k = gsl_vector_get(vx, i);
			//double y_k = gsl_vector_get(vy, i);
			//double f_x_k = ann_feed_forward(network, x_k);
			//s += fabs(f_x_k-y_k);
			double dy_k = gsl_vector_get(vdy, i);
			double df_x_k = ann_feed_forward_derivative(network, df, x_k);
			s += fabs(df_x_k-dy_k);
		}
		return s/vx->size;
	}

	gsl_vector * p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p, network->data);
	int nsteps = qnewton(delta, p, 1e-3);	//remember that here the minimum is found through these Newton steps! We used this symmetric Broydens update for the Hessian matrix.
	gsl_vector_memcpy(network->data, p);
	gsl_vector_free(p);
return nsteps;
}
*/

