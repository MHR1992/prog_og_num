#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>

#define RND ((double) rand()/RAND_MAX)

//gives x_i a random value betweeen a_i and b_i.
void randomx(int dim, gsl_vector * a, gsl_vector * b, gsl_vector * x){
	double a_i, b_i;
	for(int i = 0; i < dim; i++){
		a_i = gsl_vector_get(a, i);
		b_i = gsl_vector_get(b, i);
		gsl_vector_set(x, i, a_i+RND*(b_i-a_i));
	}
}


void plainmc(int dim, int N, gsl_vector * a, gsl_vector * b, double f(gsl_vector * x),
	double * result, double * err){

	//calculates Volume
	double Volume = 1, a_i, b_i;
	for(int i = 0; i < dim; i++){
		a_i = gsl_vector_get(a, i);
		b_i = gsl_vector_get(b, i);
		Volume *= b_i-a_i;
	}

	//finding f^2 and f to be able to find <f>^2 and <f^2> 
	double sum_f = 0, sum_f_sqrd = 0, fx;
	gsl_vector * x = gsl_vector_alloc(dim);
	for(int i = 0; i < N; i++){
		randomx(dim, a, b, x);
		fx = f(x);
		sum_f += fx;
		sum_f_sqrd += fx*fx;
	}
	double mean = sum_f/N;				//<f_i>
	double variance = sum_f_sqrd/N-mean*mean;	//<f_i²>-<f_i>²
	* result = mean*Volume;				//(2)
	* err = sqrt(variance/N)*Volume;		//(3)

	gsl_vector_free(x);
}
