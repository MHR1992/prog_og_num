#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include<assert.h>
#include<math.h>

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){
	free(v->data); 
	free(v);
}
/* v->data is identical to (*v).data */

void nvector_set(nvector* v, int i, double value){
	assert(0 <= i && i < v->size);
	v->data[i] = value;
}

double nvector_get(nvector* v, int i){
	return (*v).data[i];
}

int double_equal(double a, double b){
        double TAU = 1e-6, EPS = 1e-6;
        if (fabs(a - b) < TAU)
                return 1;
        if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
                return 1;
        return 0;
}

double nvector_dot_product(nvector * v, nvector * u){
	assert(v->size == u->size);
	double d_prod = 0;
	for (int i = 0; i < v->size; i++){
		double product_i = ((*v).data[i])*((*u).data[i]);
		d_prod += product_i;
	}
	return d_prod;
}

void nvector_print(char * s, nvector * v){
        printf("%s", s);
        for (int i = 0; i < v->size; i++)
                printf("%9.3g ", v->data[i]);
        printf("\n");
}

void nvector_add(nvector * a, nvector * b){
        assert(a->size == b->size);
        for (int i = 0; i < a->size; i++){
                double s = nvector_get(a, i) + nvector_get(b, i);
                nvector_set(a, i, s);
        }
}




int nvector_equal(nvector* v, nvector* u){
	double acc = 1e-6; double eps = 1e-6;
	if (v->size != u->size) return 0;
	for(int i = 0; i < v->size; i++){
		double vi = v->data[i];
		double ui = u->data[i];
		if(!(fabs(vi-ui) < acc && fabs(vi-ui)/(fabs(vi)+fabs(ui)) < eps)) return 0;
	}
	return 1;
}



/* ... */


