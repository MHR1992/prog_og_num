#include<stdio.h>
#include<unistd.h>
//#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>


int sine_ode(double phi, const double y[], double y_prime[], void * params){
	y_prime[0] = y[1];
	y_prime[1] = -y[0];
	return GSL_SUCCESS;
}

int main(){

	gsl_odeiv2_system orbit;
        orbit.function = sine_ode;
        orbit.jacobian = NULL;
        orbit.dimension = 2;

        double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
        double delta_phi = 0.05;

        gsl_odeiv2_driver * driver =
                gsl_odeiv2_driver_alloc_y_new(&orbit, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

        double t = 0, y[2] = {0, 1};
        for (double phi = 0; phi < 2*M_PI; phi += delta_phi) {
                int status = gsl_odeiv2_driver_apply (driver, &t, phi, y);
		double sin2 = sin(phi);
		printf ("%g %g %g\n", phi, y[0], sin2);
                if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
                }

        gsl_odeiv2_driver_free (driver);

}
