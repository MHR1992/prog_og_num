#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

int jacobi(gsl_matrix * A, gsl_vector * e, gsl_matrix * V){

	int changed, sweeps = 0, n = A->size1;

	for(int i = 0; i < n; i++){							//setting diagonal of matrix A into vector e
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}

	gsl_matrix_set_identity(V);							//sets V to identity matrix

	do{
		changed = 0; sweeps++; int p, q;					//pull "=0" up to decl maybe
		for(p = 0; p < n; p++){							//top row: p
			for(q = p+1; q < n; q++){					//bottom row: q
				double app = gsl_vector_get(e, p);			//app = p'th diagonal element
				double aqq = gsl_vector_get(e, q);			//aqq = q'th diagonal element
				double apq = gsl_matrix_get(A, p, q);
				double phi = 0.5*atan2(2*apq, aqq-app);			//atan2(y,x)=tan⁻¹(y/x) (equation (10))
				double c = cos(phi), s = sin(phi);
				double app1 = c*c*app-2*s*c*apq+s*s*aqq;		//the updated app (A_pp' in notes) is calculated
				double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;		//updated aqq
				if(app1 != app || aqq1 != aqq){
					changed = 1;
					gsl_vector_set(e, p, app1);			//updating diagonal with A_pp'
					gsl_vector_set(e, q, aqq1);
					gsl_matrix_set(A, p, q, 0.0);			//from (6) only upper part evaluated so only 1
					for(int i = 0; i < p; i++){
						double aip = gsl_matrix_get(A, i, p);	//updating elements from equation (9)
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*aip-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*aip);
					}
					for(int i = p+1; i < q; i++){
						double api = gsl_matrix_get(A, p, i);
						double aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*api-s*aiq);
						gsl_matrix_set(A, i, q, c*aiq+s*api);
					}
					for(int i = q+1; i < n; i++){
						double api = gsl_matrix_get(A, p, i);
						double aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*api-s*aqi);
						gsl_matrix_set(A, q, i, c*aqi+s*api);
					}
					for(int i = 0; i < n; i++){
						double vip = gsl_matrix_get(V, i, p);
						double viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*vip-s*viq);
						gsl_matrix_set(V, i, q, c*viq+s*vip);
					}
				}
			}
		}
	} while(changed != 0);
	return sweeps;
}

