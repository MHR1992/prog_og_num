#ifndef HAVE_QR_GS_H
#define HAVE_QR_GS_H
#include<stdlib.h>
#include"mv_operations.h"

void qr_gs_decomp(matrix * A, matrix * R);
void qrbacksub(matrix * R, vector * x);
void qr_gs_solve(vector * x, matrix * Q, matrix * R, vector * b);
void qrinv(matrix * Q, matrix * R, matrix * B);

#endif
