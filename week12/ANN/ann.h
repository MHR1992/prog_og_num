#include<gsl/gsl_vector.h>
#ifndef HAVE_NEURONS
#define HAVE_NEURONS
//we make a new type called ann, where n is # of neurons in the hidden layer,
//f is the activation function and
//gsl_vector * data keeps the params {ai, bi, wi} for each n
typedef struct{
	int n;
	double (*f)(double);
	gsl_vector * data;
	} ann;

ann * ann_alloc(int n, double (*f)(double));
void ann_free(ann * network);
double ann_feed_forward(ann * network, double x);
double ann_feed_forward_derivative(ann * network, double (* d_af)(double),double x);
double ann_feed_forward_antiderivative(ann * network, double (* Af)(double), double x);
int ann_train_with_derivatives(ann * network, double (*d_af)(double), gsl_vector * vx, gsl_vector * vy, gsl_vector * vdy);
int ann_train(ann * network, gsl_vector * vx, gsl_vector * vy);
#endif

