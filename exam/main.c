#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs_gsl.h"
#include"operations.h"
#define RND ((double)rand()/RAND_MAX)

void inverse_power_method(gsl_matrix * A, double s_init, gsl_vector * v, int rayleigh_switch, double abs, double rel);
int jacobi(gsl_matrix * A, gsl_vector * e, gsl_matrix * V);

int main(int argc, char** argv){
	int rayleigh_switch = argc > 1 ? atoi(argv[1]) : 1;
	
	int n = 5;									//dim of matrix
	double abstol = 1e-6, reltol = 1e-6;

	//making random matrix A and A2 (to use for jacobi)
	gsl_matrix * A = gsl_matrix_alloc(n, n);
	gsl_matrix * A2 = gsl_matrix_alloc(n, n);
	set_gsl_matrix_random(A);
	gsl_matrix_memcpy(A2, A);
	printf("Matrix A:\n");
	print_gsl_matrix(A);

	//finding the eigevals with jacobi
	gsl_matrix * V = gsl_matrix_alloc(n, n);
	gsl_vector * eigvals = gsl_vector_alloc(n);
	jacobi(A, eigvals, V);
	gsl_matrix_memcpy(A, A2);
	printf("\nEigenvectors from jacobi:\n");
	print_gsl_matrix(V);
	printf("\nEigenvalues from jacobi:\n");
	gsl_vector_fprintf(stdout, eigvals, "%g");


	//guess on eigenvalue and creation of random initial x_i
	double s_init = 0.2;
	gsl_vector * x_init = gsl_vector_alloc(n);
	gsl_vector * x_eigv = gsl_vector_alloc(n);
	set_gsl_vector_random(x_init);

	printf("\nThe initial random vector; x_init:\n"); gsl_vector_fprintf(stdout, x_init, "%g");
	printf("\nThe initial guess of eigenvalue (s):\n %g", s_init);

	//finding eigenval closest to s_init (most likely)
	//set rayleigh_switch = 0 if convergence to closest eigevalue is
	//more important than fast convergence to eigenvalue!
	printf("\n\nINVERSE RAYLEIGH QUOTIENT ITERATION:\n");
	printf("\nRayleigh_switch = %i\n", rayleigh_switch);
	inverse_power_method(A, s_init, x_init, rayleigh_switch, abstol, reltol);
	printf("If one wants slower convergence but more accurate method with regard to finding the closest eigenvalue turn rayleigh_switch off (set rayleigh_switch = 0).\n\n");
	//free
	gsl_matrix_free(A);
	gsl_matrix_free(A2);
	gsl_matrix_free(V);
	gsl_vector_free(eigvals);
	gsl_vector_free(x_init);
	gsl_vector_free(x_eigv);

	return 0;
}
