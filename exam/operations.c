#include<gsl/gsl_matrix.h>
#include<math.h>
#define RND ((double)rand()/RAND_MAX)

void print_gsl_matrix(gsl_matrix * M){
        for(int i = 0; i < M->size1; i++){
                for(int j = 0; j < M->size2; j++){
                        double M_ij = gsl_matrix_get(M, i, j);
                        printf("%g\t", M_ij);
                }
		printf("\n");
        }
}
void set_gsl_vector_random(gsl_vector * v){
   for(int i = 0; i < v->size; i++) gsl_vector_set(v, i, RND);
}

void set_gsl_matrix_random(gsl_matrix * A){
     for(int i = 0; i < A->size1; i++)for(int j = i; j < A->size2; j++){
                double rnd = RND;
                gsl_matrix_set(A, i, j, rnd); gsl_matrix_set(A,j, i, rnd);
        }
}

int check_gsl_vectors_equal(gsl_vector * x, gsl_vector * y, double abs, double rel){
        if(x->size != y->size) return 0;
        int absresult = 1;
        int relresult = 1;
        for(int i = 0; i < x->size; i++){
                double xi = gsl_vector_get(x, i);
                double yi = gsl_vector_get(y, i);
                absresult = absresult && (fabs(xi-yi) <= abs);
                relresult = relresult && (fabs(xi-yi)/(fabs(xi)+fabs(yi)) <= rel/2);
        }
        return absresult || relresult;
}

