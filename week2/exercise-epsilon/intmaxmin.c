#include<stdio.h>
#include<limits.h>
#include<float.h>


int intmaxmin() {
	printf("\nExercise 1i:\n");
	int i = 1;
	while(i+1 > i){i++;}
	printf("with 'while' loop we get %i\n", i);
	if(i == INT_MAX){printf("and it is equal to INT_MAX\n");}
	int j = 1;
	do {j++;} while(j+1 > 1);
	printf("with 'do while' loop we get %i\n",j);
	if(j == INT_MAX){printf("and it is equal to INT_MAX\n");}
	int k = 1;
	for(k; (k+1) > k; k++);
	printf("with 'for' loop we get %i\n", k);
	if(k == INT_MAX){printf("and it is equal to INT_MAX\n");}

	printf("\nExercise 1ii:\n");
	i = 1;
	while(i-1<i){i--;};
	printf("with 'while' loop we get %i\n",i);
	if(i==INT_MIN){printf("and it is equal to INT_MIN\n");}
	i = 1;
	do{i--;} while(i-1<i);
	printf("with 'do while' loop we get %i\n",i);
	if(i==INT_MIN){printf("and it is equal to INT_MIN\n");}
	i = 1;
	for(i; i-1<i;){i--;};
	printf("with 'for' loop we get %i\n",i);
	if(i==INT_MIN){printf("and it is equal to INT_MIN\n");}

	printf("\nExercise 1iii:\n");
	double x = 1;
	while(1+x!=1){x/=2;};
	printf("found double machine epsilon is %g\n",2*x);
	if(2*x==DBL_EPSILON) {printf("and it is the real value\n");}
	float y = 1;
	while(1+y!=1){y/=2;};
	printf("found float machine epsilon is %g\n",2*y);
	if(2*y==FLT_EPSILON) {printf("and it is the real value\n");}
	long double z = 1;
	while(1+z!=1){z/=2;};
	printf("found long double machine epsilon is %Lg\n",2*z);
	if(2*z==LDBL_EPSILON) {printf("and it is the real value\n");}

return 0;
}
