#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf_airy.h>
#include<gsl/gsl_monte_vegas.h>
int main(){
	for(double i = -2*M_PI; i < 2*M_PI; i += 0.01)
		printf("%g %g %g\n", i, gsl_sf_airy_Ai(i,GSL_VEGAS_MODE_IMPORTANCE),
		gsl_sf_airy_Bi(i,GSL_VEGAS_MODE_IMPORTANCE));
return 0;
}
