#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"simple_operations.h"
#define FMT "%7.3f"  // .3 is still the decimal precision and 7 is the spaces reserved for the output


//prints the matrix
void matrix_print(gsl_matrix * A){
        for(int r = 0; r < A->size1; r++){
                printf(" ");
                for(int c = 0; c < A->size2; c++){
                        fprintf(stderr, FMT, gsl_matrix_get(A,r,c));
                }
                fprintf(stderr, "\n");
        }
}

//prints vector
void vector_print(gsl_vector * v){
        for(int i = 0; i < v->size; i++){
                fprintf(stderr, FMT, gsl_vector_get(v, i ));
        }
}


