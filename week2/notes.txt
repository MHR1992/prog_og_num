Memory allocation
 - Memory leak
 - malloc function
 - free function
 - garbage collection

Pointers
 - Dynamic memory allocation cannot happen without using pointers. Every
memory location has its address. A variables address can be accesed by 
the prefix "&"! A pointer is a variable whos value is the address of another
variable. 
 - Why is double *data the same as data[]?
 - (*v) is content - can use -> instead


Void in front of function call is to ensure that it does not return anything
to you (as integer for e.g main function). We just need to initialize the
function in some cases.
