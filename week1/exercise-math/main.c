#include<complex.h>
#include<math.h>
#include<stdio.h>
int main() {
	printf("\nExercise 1\n");
	printf("tgamma(5) is: %f\n", tgamma(5));
	printf("Besselfunction j(0.5) is: %f\n", j1(0.5));
	printf("real part of sqrt(-2): %f", creal(csqrt(-2)));
	printf("real part of exp(i): %f\n", creal(cexp(I)));
	printf("real part of exp(pi*i) %f\n", creal(cexp(I*M_PI)));
	printf("real part of i^e: %f\n", creal(cpow(I, M_E)));
	printf("One could also get imaginary part by using 'cimag'.");

	printf("\n\nExercise 2:\n");
	int bits = sizeof(void *)*8;
	printf("\nThis is (probably a %i bit system...", bits);
	printf("\nfloat (1.0f/9): %.25g\n", 1.0f/9);
	printf("\ndouble (1.0/9): %.25lg\n", 1.0/9);
	printf("\nlong double (1.0L/9): %.25Lg\n", 1.0L/9);

	return 0;
}

