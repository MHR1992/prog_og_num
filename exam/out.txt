Matrix A:
0.840188	0.394383	0.783099	0.79844	0.911647	
0.394383	0.197551	0.335223	0.76823	0.277775	
0.783099	0.335223	0.55397	0.477397	0.628871	
0.79844	0.76823	0.477397	0.364784	0.513401	
0.911647	0.277775	0.628871	0.513401	0.95223	

Eigenvectors from jacobi:
-0.284865	0.756261	0.168984	-0.0873305	0.55744	
-0.624185	-0.327753	-0.217449	0.610858	0.287297	
0.0720496	-0.465468	0.769646	-0.099409	0.419417	
0.723837	0.0659931	-0.189576	0.511012	0.417892	
0.0107027	-0.315645	-0.543936	-0.590094	0.506139	

Eigenvalues from jacobi:
-0.556796
-0.123545
0.069162
0.460944
3.05896

The initial random vector; x_init:
0.916195
0.635712
0.717297
0.141603
0.606969

The initial guess of eigenvalue (s):
 0.2

INVERSE RAYLEIGH QUOTIENT ITERATION:

Rayleigh_switch = 1

Converged to eigenvalue: 0.069162 after 6 iterations. 

Final eigenvector with above eigenvalue: 
0.168984
-0.217449
0.769646
-0.189576
-0.543936

If one wants slower convergence but more accurate method with regard to finding the closest eigenvalue turn rayleigh_switch off (set rayleigh_switch = 0).

