#include<stdio.h>
#include<assert.h>
#include<qr_gs.h>
#include<mv_operations.h>

void lsfit(int m, double f(int i, double x), vector * x, vector * y, vector * dy,
	vector * c, matrix * S){
	int n = x->size;

	matrix * A    = matrix_alloc(n,m);
	vector * b    = vector_alloc(n);
	matrix * R    = matrix_alloc(m,m);
	matrix * R_inv = matrix_alloc(m,m);
	matrix * I    = matrix_alloc(m,m);

	for(int i = 0; i < n; i++){
		double xi  = vector_get(x ,i);
		double yi  = vector_get(y ,i);
		double dyi = vector_get(dy, i);
		assert(dyi > 0);
		vector_set(b, i, yi/dyi);					//see (7)
		for(int k = 0; k < m; k++) matrix_set(A, i, k, f(k, xi)/dyi);	//see (7)
	}
	qr_gs_decomp(A, R);							//see (3) A turns into Q
	qr_gs_solve(c, A, R, b);						//see (4)

	matrix_set_identity(I);
	qrinv(I, R, R_inv);
	matrix * R_invT = matrix_transpose(R_inv);
	matrix_mm(S, R_inv, R_invT);

	matrix_free(A);
	vector_free(b);
	matrix_free(R);
	matrix_free(R_inv);
	matrix_free(I);
	matrix_free(R_invT);

}
