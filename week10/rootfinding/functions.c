#include<math.h>
#include"mv_operations.h"

void f_A2(vector * v, vector * fx, matrix * J, int * ncalls_A2){
	(*ncalls_A2)++;
	double A = 10000;
	double x = vector_get(v, 0), y = vector_get(v, 1);
	vector_set(fx, 0, A*x*y-1);
	vector_set(fx, 1, exp(-x)+exp(-y)-1-1.0/A);
	matrix_set(J, 0, 0, A*y);
	matrix_set(J, 0, 1, A*x);
	matrix_set(J, 1, 0, -exp(-x));
	matrix_set(J, 1, 1, -exp(-y));
}

void f_A3(vector * v, vector * fx, matrix * J, int * ncalls_A3){
//	printf("%g", *ncalls_A3);
	(*ncalls_A3)++;
//	printf("%g", *ncalls_A3);
	double x = vector_get(v, 0), y = vector_get(v, 1);
	vector_set(fx, 0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
	vector_set(fx, 1, 100*2*(y-x*x));
	matrix_set(J, 0, 0, 1200*x*x-400*y+2); 
	matrix_set(J, 0, 1, -400*x);
	matrix_set(J, 1, 0, -400*x);
	matrix_set(J, 1, 1, 200);
}

void f_A4(vector * v, vector * fx, matrix * J, int * ncalls_A4){
	(*ncalls_A4)++;
	double x = vector_get(v, 0), y = vector_get(v, 1);
	vector_set(fx, 0, 2*(2*x*(x*x+y-11)+x+y*y-7));
	vector_set(fx, 1, 2*(x*x+2*y*(x+y*y-7)+y-11));
	matrix_set(J, 0, 0, 12*x*x+4*y-42); 
	matrix_set(J, 0, 1, 4*x+4*y);
	matrix_set(J, 1, 0, 4*x+4*y);
	matrix_set(J, 1, 1, 12*y*y+4*x-26);
}

void f_B2(vector * p, vector * fx, int * ncalls_B2){
	(*ncalls_B2)++;
	double A = 10000;
	double x = vector_get(p, 0), y = vector_get(p, 1);
	vector_set(fx, 0, A*x*y-1);
	vector_set(fx, 1, exp(-x)+exp(-y)-1-1.0/A);
}

void f_B3(vector * p, vector * fx, int * ncalls_B3){
	(*ncalls_B3)++;
	double x = vector_get(p, 0), y = vector_get(p, 1);
	vector_set(fx, 0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);         //setting up system of non-line$                vector_set(fx, 1, 100*2*(y-x*x));
	vector_set(fx, 1, 100*2*(y-x*x));

}

void f_B4(vector * p, vector * fx, int * ncalls_B4){
	(*ncalls_B4)++;
	double x = vector_get(p, 0), y = vector_get(p, 1);
	vector_set(fx, 0, 2*(2*x*(x*x+y-11)+x+y*y-7));
	vector_set(fx, 1, 2*(x*x+2*y*(x+y*y-7)+y-11));
}
