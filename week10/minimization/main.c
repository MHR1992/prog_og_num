#include"mv_operations.h"
#include<math.h>
#include<stdio.h>

int qnewton(double cost(vector * x), vector * x, double eps);
int newton(double f(vector * x, vector * df, matrix * H), vector * x, double acc);
void numeric_gradient(double cost(vector * x), vector * x, vector * g);

double rosen(vector * v){
	double x = vector_get(v, 0);
	double y = vector_get(v, 1);
	return pow(1-x, 2)+100*pow(y-x*x, 2);
}

double rosen_all(vector * v, vector * df, matrix * H){
	double x = vector_get(v, 0);
	double y = vector_get(v, 1);
	vector_set(df, 0, -2*(1-x)-400*x*(y-x*x));
	vector_set(df, 1, 200*(y-x*x));
	matrix_set(H, 0, 0, 2-400*y+1200*x*x);
	matrix_set(H, 0, 1, -400*x);
	matrix_set(H, 1, 0, -400*x);
	matrix_set(H, 1, 1, 200);
	return pow(1-x, 2)+100*pow(y-x*x, 2);
}

double himmel(vector * v){
        double x = vector_get(v, 0);
        double y = vector_get(v, 1);
        return pow(x*x+y-11, 2)+pow(x+y*y-7, 2);
}

double himmel_all(vector * v, vector * df, matrix * H){
	double x = vector_get(v, 0);
        double y = vector_get(v, 1);
        vector_set(df, 0, 2*2*x*(x*x+y-11)+2*(x+y*y-7));
	vector_set(df, 1, 2*(x*x+y-11)+2*2*y*(x+y*y-7));
	matrix_set(H, 0, 0, 12*x*x+4*y-42);
	matrix_set(H, 0, 1, 4*x+4*y);
	matrix_set(H, 1, 0, 4*x+4*y);
	matrix_set(H, 1, 1, 4*x+12*y*y-26);
        return pow(x*x+y-11, 2)+pow(x+y*y-7, 2);
}

double radsubfit(vector * params){
	double A = vector_get(params, 0);
	double T = vector_get(params, 1);
	double B = vector_get(params, 2);
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
	double result = 0;
	for(int i = 0; i < N; i++){
		result += pow(A*exp(-t[i]/T)+B-y[i], 2)/e[i]/e[i];
	}
	return result;
}

int main(){
	int n = 2, nsteps;
	vector * x = vector_alloc(n);
	vector * g = vector_alloc(n);
	vector * ros_df = vector_alloc(n);
	vector * him_df = vector_alloc(n);
	matrix * ros_H = matrix_alloc(n, n);
	matrix * him_H = matrix_alloc(n, n);

	double eps = 1e-4;
	double x0 = 2;
	double x1 = -3;
	printf("eps = %.1e\n", eps);

	printf("\nEXERCISE A - Newtons minimization method:\n\n");

	printf("\nA: MIN OF ROSENBROCK'S FUNCTION\n");
	printf("Start point (x): \n");
	vector_set(x, 0, x0);
	vector_set(x, 1, x1);
	vector_print(x);
	printf("Function value at start point:\n %g\n", rosen_all(x, ros_df, ros_H));
	nsteps = newton(rosen_all, x, eps);
	printf("Steps:\n %i\n", nsteps);
	printf("Minimum found at: \n");
	vector_print(x);
	printf("Function value at minimum:\n %7.5g\n", rosen_all(x, ros_df, ros_H));
	printf("Gradient at minimum:\n");
	vector_print(ros_df);

	printf("\nA: MIN OF HIMMELBLAU'S FUNCTION\n");
	printf("Start point (x): \n");
	vector_set(x, 0, x0);
	vector_set(x, 1, x1);
	vector_print(x);
	printf("Function value at start point:\n %g\n", himmel_all(x, him_df, him_H));
	nsteps = newton(himmel_all, x, eps);
	printf("Steps:\n %i\n", nsteps);
	printf("Minimum found at: \n");
	vector_print(x);
	printf("Function value at minimum:\n %7.5g\n", himmel_all(x, him_df, him_H));
	printf("Gradient at minimum:\n");
	vector_print(him_df);

	printf("\nEXERCISE B - Quasi-Newton method with Broyden\n");

	printf("\nB.ii: MIN OF ROSENBROCK'S FUNCTION\n");
	printf("Start point (x): \n");
	vector_set(x, 0, x0);
	vector_set(x, 1, x1);
	vector_print(x);
	printf("Function value at start point:\n %g\n", rosen(x));
	nsteps = qnewton(rosen, x, eps);
	printf("Steps:\n %i\n", nsteps);
	printf("Minimum found at: \n");
	vector_print(x);
	printf("Function value at minimum:\n %g\n", rosen(x));
	numeric_gradient(rosen, x, g);
	printf("Gradient at minimum:\n");
	vector_print(g);

	printf("\nB.ii: MIN OF HIMMELBLAU'S FUNCtION\n");
	printf("Start point (x):\n");
	vector_set(x, 0, x0);
	vector_set(x, 1, x1);
	vector_print(x);
	printf("Function value at start point:\n %g\n", himmel(x));
	nsteps = qnewton(himmel, x, eps);
	printf("Steps:\n %i\n", nsteps);
	printf("Minimum found at:\n");
	vector_print(x);
	printf("Function value at minimum:\n %g\n", himmel(x));
	numeric_gradient(himmel, x, g);
	printf("Gradient at minimum:\n");
	vector_print(g);

	printf("\nB.iii:\nThe number of steps with Jacobian (rootfinding) was:\n%i (Rosen)\n%i (Himmel)\n", 1242, 12);
	printf("\nThe number of steps without Jacobian (rootfinding) was:\n%i (Rosen)\n%i (Himmel)\n", 63, 22);
	printf("\nFrom the number of steps it therefore seems as if this Newton method is the most effective!\n");

	printf("\nB.iv: MIN OF ki² FUNCtION\n");
	printf("Start point (x):\n");
	vector * params = vector_alloc(3);
	vector * gradofparams = vector_alloc(3);
	vector_set(params, 0, 1);
	vector_set(params, 1, 1);
	vector_set(params, 2, 1);
	vector_print(params);
	printf("Function value at start point:\n %g\n", radsubfit(params));
	nsteps = qnewton(radsubfit, params, eps);
	printf("Steps:\n %i\n", nsteps);
	printf("Minimum found at:\n");
	vector_print(params);
	printf("Function value at minimum:\n %g\n", radsubfit(params));
	numeric_gradient(radsubfit, params, gradofparams);
	printf("Gradient at minimum:\n");
	vector_print(gradofparams);
	printf("\nSee the svg-file for the experimental data and the best fitting function.\n");
	printf("\n\n");
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	for(int i = 0; i < N; i++){
		printf("%5.2g %5.2g %5.2g\n", t[i], y[i], e[i]);
	}
	printf("\n\n");
	double A = vector_get(params, 0);
	double T = vector_get(params, 1);
	double B = vector_get(params, 2);
	for(int i = 0; i < 100; i ++){
		double ti = i/10.0;
		double fit_res = A*exp(-ti/T)+B;
		printf("%5.2g %10.5g\n", ti, fit_res);
	}


return 0;
}
