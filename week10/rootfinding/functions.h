#ifndef HAVE_FUNCTIONS_H
#define HAVE_FUNCTIONS_H
#include"mv_operations.h"
void f_A2(vector * v, vector * fx, matrix * J, int * ncalls_A2);

void f_A3(vector * v, vector * fx, matrix * J, int * ncalls_A3);

void f_A4(vector * v, vector * fx, matrix * J, int * ncalls_A4);

void f_B2(vector * v, vector * fx, int * ncalls_B2);

void f_B3(vector * p, vector * fx, int * ncalls_B3);

void f_B4(vector * p, vector * fx, int * ncalls_B4);

#endif
