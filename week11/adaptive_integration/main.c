#include<math.h>
#include<assert.h>
#include<stdio.h>
#include<gsl/gsl_integration.h>

double adapt(double f(double),double a,double b,double acc,double eps);
double clenshaw_curtis(double f(double),double a,double b,double acc,double eps);

int main(){		//uses gcc nested functions

	int calls = 0;
	double a = 0, b = 1, acc = 1e-4, eps = 1e-4;
	double s(double x){
		calls++;
		return sqrt(x);
	}
	double exact = 2./3; calls = 0;
	double Q = adapt(s, a, b, acc, eps);
	printf("EXERCISE A & B (presented together)\n\n");
	printf("\nA: Open Qandrature: Integral of sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	calls = 0;
	Q = clenshaw_curtis(s, a, b, acc, eps);
	exact = 2./3;
	printf("\nB: Clenshaw-Curtis: Integral of sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));


	calls = 0;
	a = 0, b = 1, acc = 1e-4, eps = 1e-4;
	double f(double x){calls++; return 1/sqrt(x);}; //nested function
	calls = 0; exact = 2;
	Q = adapt(f, a, b, acc, eps);
	printf("\nA: Open Qandrature: Integral of 1/sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numereical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	calls = 0; exact = 2;
	Q = clenshaw_curtis(f, a, b, acc, eps);
	printf("\nB: Clenshaw-Curtis: Integral of 1/sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	a = 0, b = 1, acc = 1e-4, eps = 1e-4;
	double f2(double x){
		calls++;
		return log(x)/sqrt(x);};
	calls = 0;
	Q = adapt(f2, a, b, acc, eps);
	exact = -4;
	printf("\nA: Open Qandratire: Integral of log(x)/sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	calls = 0; exact = -4;
	Q = clenshaw_curtis(f2, a, b, acc, eps);
	printf("\nB: Clenshaw-Curtis: Integral of log(x)/sqrt(x) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	a = 0, b = 1, acc = 1e-4, eps = 1e-4;
	double f3(double x){
		calls++;
		return 4*sqrt( 1-pow((1-x), 2) );};
	calls = 0;
	Q = adapt(f3, a, b, acc, eps);
	exact = M_PI;
	printf("\nOpen Qandratire: Integral of 4*sqrt(1-(1-x)²) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error (tolerance) = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));

	calls = 0; exact = M_PI;
	Q = clenshaw_curtis(f3, a, b, acc, eps);
	printf("\nB: Clenshaw-Curtis: Integral of 4*sqrt(1-(1-x)²) from %g to %g\n", a, b);
	printf("acc = %g eps = %g\n", acc, eps);
	printf("Q (numerical integral) = %g\n", Q);
	printf("exact integral = %g\n", exact);
	printf("number of calls = %d\n", calls);
	printf("estimated error = %g\n", acc+fabs(Q)*eps);
	printf("actual error = %g\n", fabs(Q-exact));
	printf("\nGenerally we see that on integrals with integrable divergencies there is a huge improvement\n when using Clenshaw-Curtis variable transformation.\n");

	printf("\n\nB: GSL ROUTINES (specifically qags)");

	gsl_function F; double result, err_gsl;
	gsl_integration_workspace * wspace = gsl_integration_workspace_alloc(10000);
	F.params = NULL;

	F.function = &s; calls = 0;
	gsl_integration_qags(&F, a, b, acc, eps, 10000, wspace, &result, &err_gsl);
	printf("\nGSL - Integral of sqrt(x) from 0 to 1\n");
	printf("result: %g\n", result);
	printf("estimated error = %g\n", err_gsl);
	printf("number of calls: %i\n", calls);

	F.function = &f; calls = 0;
	gsl_integration_qags(&F, a, b, acc, eps, 10000, wspace, &result, &err_gsl);
	printf("\nGSL - Integral of 1/sqrt(x) from 0 to 1\n");
	printf("result: %g\n", result);
	printf("estimated error = %g\n", err_gsl);
	printf("number of calls: %i\n", calls);

	F.function = &f2; calls = 0;
	gsl_integration_qags(&F, a, b, acc, eps, 10000, wspace, &result, &err_gsl);
	printf("\nGSL - Integral of ln(x)/sqrt(x) from 0 to 1\n");
	printf("result: %g\n", result);
	printf("estimated error = %g\n", err_gsl);
	printf("number of calls: %i\n", calls);

	F.function = &f3; calls = 0;
	gsl_integration_qags(&F, a, b, acc, eps, 10000, wspace, &result, &err_gsl);
	printf("\nGSL - Integral of 4*sqrt(1-(1-x)²) from 0 to 1\n");
	printf("result: %g\n", result);
	printf("estimated error = %g\n", err_gsl);
	printf("number of calls: %i\n", calls);
	printf("\nWe see that the Clenshaw-Curtis adaptive integration algorithm alctually \nperforms better than qags from the GSL library!\n.Also note that one could have used qagp, which uses known singular point.\nHowever, for a more 'fair' comparison this was not used.\n\n");

	//free
	gsl_integration_workspace_free(wspace);

	return 0;
}
