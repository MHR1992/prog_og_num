#ifndef HAVE_OPERATIONS_H
#define HAVE_OPERATIONS_H
#include<stdlib.h>

typedef struct {int size1, size2; double * data;} matrix;
typedef struct {int size; double * data;} vector;

//matrix operations
matrix * 	matrix_alloc(int n, int m);
void 		matrix_free(matrix * A);
void 		matrix_set(matrix * A, int i, int j, double x);
double 		matrix_get(matrix * A, int i, int j);
void 		matrix_print(matrix * A);
void 		matrix_random(matrix * A);
matrix *	matrix_cpy(matrix * A);
void		matrix_cpy2(matrix * A, matrix * B);
vector * 	matrix_get_column(matrix * A, int i);
void		matrix_get_column2(matrix * A, int j, vector * v);
void 		matrix_set_column(matrix * A, int j, vector * v);
matrix *	matrix_transpose(matrix * A);
void		matrix_mm(matrix * AB, matrix * A, matrix * B);
void 		matrix_set_identity(matrix * A);
void 		matrix_add_outprod(matrix * B, double a, vector * v1, vector * v2);
void		matrix_get_row(vector * v, matrix * A, int i);

//matrix-vector opeartions
void 		matrix_vector_dot(vector * x, matrix * A, const char T, vector * y);
void 		matrix_vector_dot2(vector * res, double a, matrix * A, const char T, vector * y);
void 		matrix_vector_add_dot(vector * res, double b, double a, matrix * A, const char T, vector * v);

//vector operations
vector * 	vector_alloc(int n);
void	 	vector_free(vector * v);
double 		vector_get(vector * v, int i);
void		vector_set(vector * v, int i, double x);
void		vector_set2(vector * v, int i, double x);
void 		vector_print(vector * v);
double 		vector_dot(vector * v1, vector * v2);
double		vector_modulus(vector * v);
void 		vector_scale(vector * v, double);
void		vector_normalize(vector * v);
void 		vector_add(double a, vector * v1, double b, vector * v2);
void 		vector_sub(double a, vector * v1, double b, vector * v2);	//included for better understanding
void 		vector_random(vector * v);
void		vector_cpy(vector * v1, vector * v2);
#endif
