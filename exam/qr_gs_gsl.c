#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <assert.h>

//see non-gsl script (the one used in week8) for more explanations
//see page 3 below (8): A ends up as Q! R is the square matrix!
void qr_gs_decomp(gsl_matrix * A, gsl_matrix * R) {
	assert(A->size2 == R->size1 && R->size2 == R->size1);
	int m = A->size2;

	for(int i = 0; i < m; i++){
		gsl_vector_view a_i = gsl_matrix_column(A, i);		//vector view is temperary object used to operate on subset of vector elements
		double R_ii = gsl_blas_dnrm2(&a_i.vector);			//computes norm of vector

		gsl_matrix_set(R, i, i, R_ii);
		gsl_vector_scale(&a_i.vector,1/R_ii); 			//normalization: a_i becomes q_i

		for(int j = i+1; j < m; j++){
			double R_ij;
			gsl_vector_view a_j = gsl_matrix_column(A, j);

			gsl_blas_ddot(&a_i.vector, &a_j.vector, &R_ij);
			gsl_blas_daxpy(-R_ij, &a_i.vector, &a_j.vector);
			gsl_matrix_set(R, i, j, R_ij);
			gsl_matrix_set(R, j, i, 0);
		}
	}
}

void qr_gs_backsub(gsl_matrix * R, gsl_vector * x) {
	int m = R->size1;

	for(int i = m-1; i>= 0; i--){
		double sum = 0;
		for(int k = i+1; k < m; k++){
			sum += gsl_matrix_get(R, i, k)*gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, (gsl_vector_get(x, i)-sum)/gsl_matrix_get(R, i, i));
	}
}

//vector x is the one that you find that solves the equation from the note
void qr_gs_solve(gsl_vector * x, gsl_matrix * Q, gsl_matrix * R, gsl_vector * b) {
	assert(x->size == b->size);

	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	qr_gs_backsub(R,x);
}
