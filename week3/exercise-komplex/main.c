#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2};
	komplex b = {3,4};

	komplex_print("a =",a);
	komplex_print("b =",b);
	printf("\n");

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a+b should be: ", R);
	komplex_print("a+b is actually: ", r);
	if(komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed\n\n");
	else
		printf("test 'add' failed: debug me, please... \n\n");

	printf("testing komplex_div...\n");
        komplex z = komplex_div(a, b);
        komplex w = {11/25.0, 2/25.0};
        komplex_print("a/b should be: ", w);
        komplex_print("a/b is actually: ", z);
        if (komplex_equal(w, z, TINY, TINY))
                printf("test 'div' passed\n\n");
        else
                printf("test 'div' falied\n\n");


	printf("testing komplex_mul...\n");
        komplex m1 = komplex_mul(a, b);
        komplex m2 = {-5, 10};
        komplex_print("a/b should be: ", m2);
        komplex_print("a/b is actually: ", m1);
        if (komplex_equal(m1, m2, TINY, TINY))
                printf("test 'mul' passed\n\n");
        else
                printf("test 'mul' falied\n\n");


	printf("testing komplex_conjugate...\n");
        komplex c1 = komplex_conjugate(a);
        komplex c2 = {1, -2};
        komplex_print("a/b should be: ", c2);
        komplex_print("a/b is actually: ", c1);
        if (komplex_equal(c1, c2, TINY, TINY))
                printf("test 'mul' passed\n\n");
        else
                printf("test 'mul' falied\n\n");

	printf("testing komplex_cos...\n");
        komplex cos1 = komplex_cos(a);
        komplex cos2 = {2.032723007, 3.051897799};
        komplex_print("a/b should be: ", cos2);
        komplex_print("a/b is actually: ", cos1);
        if (komplex_equal(cos1, cos2, TINY, TINY))
                printf("test 'cos' passed\n\n");
        else
                printf("test 'cos' falied\n\n");

}
