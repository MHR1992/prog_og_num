#include<stdio.h>
#include"mv_operations.h"
#include"qr_gs.h"
#include<math.h>
#include"functions.h"

int newton(void f(vector * x, vector * fx, int * ncalls), vector * x, int * ncalls, double dx, double eps);
int newton_with_jacobian(void f(vector * x, vector * fx, matrix * J, int * ncalls), vector * x, int * ncalls, double dx, double eps);
int gsl_system(double x0_init, double x1_init);
int gsl_system_no_jacobian(double x0_init, double x1_init);

int gsl_rosenbrock(double x0_init, double x1_init);
int gsl_rosenbrock_no_jacobian(double x0_init, double x1_init);
int gsl_himmelb(double x0_init, double x1_init);
int gsl_himmelb_no_jacobian(double x0_init, double x1_init);

int main(){

	int ncalls_A2 = 0;
	int ncalls_A3 = 0;
	int ncalls_A4 = 0;
	int ncalls_B2 = 0;
	int ncalls_B3 = 0;
	int ncalls_B4 = 0;
	//initial x
	double x0 = 2;
	double x1 = -3;

	//allocation
	vector * x_A2 = vector_alloc(2);
	vector * x_A3 = vector_alloc(2);
	vector * x_A4 = vector_alloc(2);
	vector * x_B2 = vector_alloc(2);
	vector * x_B3 = vector_alloc(2);
	vector * x_B4 = vector_alloc(2);

	vector_set(x_A2, 0, x0); vector_set(x_A2, 1, x1);
	vector_set(x_A3, 0, x0); vector_set(x_A3, 1, x1);
	vector_set(x_A4, 0, x0); vector_set(x_A4, 1, x1);
	vector_set(x_B2, 0, x0); vector_set(x_B2, 1, x1);
	vector_set(x_B3, 0, x0); vector_set(x_B3, 1, x1);
	vector_set(x_B4, 0, x0); vector_set(x_B4, 1, x1);

	vector * fx_A2 = vector_alloc(2);
	vector * fx_A3 = vector_alloc(2);
	vector * fx_A4 = vector_alloc(2);
	vector * fx_B2 = vector_alloc(2);
	vector * fx_B3 = vector_alloc(2);
	vector * fx_B4 = vector_alloc(2);

	matrix * j_A2 = matrix_alloc(2, 2);
	matrix * j_A3 = matrix_alloc(2, 2);
	matrix * j_A4 = matrix_alloc(2, 2);

	printf("\n\n	EXERCISE A	");

	//Exercise A.2
	printf("\n\nExercise A.2 - 'a system':\n");
	printf("Initial guess of vector x:\n"); vector_print(x_A2);
	f_A2(x_A2, fx_A2, j_A2, &ncalls_A2);
	printf("Start value of f(x)):\n"); vector_print(fx_A2);
	newton_with_jacobian(f_A2, x_A2, &ncalls_A2, 1e-4, 1e-4);
	printf("number of calls: %i\n", ncalls_A2);
	//vector_fprintf(stderr, x, "%g");
	printf("solution x is:"); vector_print(x_A2);
	f_A2(x_A2, fx_A2, j_A2, &ncalls_A2);
	printf("Final f(x) is:"); vector_print(fx_A2);


	//Exercise A.3
	printf("\n\nExercise A.3 - Rosenbrock function:\n");
	printf("Initial guess of vector x:\n"); vector_print(x_A3);
	f_A3(x_A3, fx_A3, j_A3, &ncalls_A3);
	printf("Start value of f(x)):\n"); vector_print(fx_A3);
	newton_with_jacobian(f_A3, x_A3, &ncalls_A3, 1e-6, 1e-6);
	printf("number of calls: %i\n", ncalls_A3);
	//vector_fprintf(stderr, x, "%g");
	printf("solution x is:"); vector_print(x_A3);
	f_A3(x_A3, fx_A3, j_A3, &ncalls_A3);
	printf("Final f(x) is:"); vector_print(fx_A3);

	//Exercise A.4
	printf("\n\nExercise A.4 - Himmelblau's function:\n");
	printf("Initial guess of vector x:\n"); vector_print(x_A4);
	f_A4(x_A4, fx_A4, j_A4, &ncalls_A4);
	printf("Start value of f(x)):\n"); vector_print(fx_A4);
	newton_with_jacobian(f_A4, x_A4, &ncalls_A4, 1e-6, 1e-6);
	printf("number of calls: %i\n", ncalls_A4);
	//vector_fprintf(stderr, x, "%g");
	printf("solution x is:"); vector_print(x_A4);
	f_A4(x_A4, fx_A4, j_A4, &ncalls_A4);
	printf("Final f(x) is:"); vector_print(fx_A4);

	//Exercise B (2)
	printf("\n\n	EXERCISE B	");

	printf("\n\nExercise B.2 - 'a system' \n");
	printf("initial guesses of vector x: ");
	vector_print(x_B2); f_B2(x_B2, fx_B2, &ncalls_B2);
	printf("Start value of f(x)):\n"); vector_print(fx_B2);
	newton(f_B2, x_B2, &ncalls_B2, 1e-6, 1e-6);
	printf("ncalls = %i\n", ncalls_B2);
	//vector_fprintf(stderr, x, "%g");
	printf("solution x is:"); vector_print(x_B2);
	f_B2(x_B2, fx_B2, &ncalls_B2);
	printf("f(x):"); vector_print(fx_B2);


	//Exercise B (3)
	printf("\n\nExercise B.2 - Rosenbrock function \n");
	printf("initial guesses of vector x: ");
	vector_print(x_B3); f_B3(x_B3, fx_B3, &ncalls_B3);
	printf("Start value of f(x)):\n"); vector_print(fx_B3);
	newton(f_B3, x_B3, &ncalls_B3, 1e-6, 1e-6);
	printf("ncalls = %i\n", ncalls_B3);
	//vector_fprintf(stderr, x, "%g");
	printf("solution x is:"); vector_print(x_B3);
	f_B3(x_B3, fx_B3, &ncalls_B3);
	printf("f(x):"); vector_print(fx_B3);

	//Exercise B (4)
	printf("\n\nExercise B.2 - Himmelblaus function \n");
	printf("Initial guesses of vector x: ");
	vector_print(x_B4); f_B4(x_B4, fx_B4, &ncalls_B4);
	printf("Start value of f(x)):\n"); vector_print(fx_B4);
	newton(f_B4, x_B4, &ncalls_B4, 1e-6, 1e-6);
	printf("ncalls = %i\n", ncalls_B4);
	//vector_fprintf(stderr, x, "%g");
	printf("Solution x is:"); vector_print(x_B4);
	f_B4(x_B4, fx_B4, &ncalls_B4);
	printf("Final f(x) is:"); vector_print(fx_B4);

	//Exercise B - gsl
	printf("\n\nExercise B.3 - 'a system' gsl\n");
	gsl_system(x0, x1);
	gsl_system_no_jacobian(x0, x1);
	printf("\n\nExercise B.3 - Rosenbrock gsl\n");
	gsl_rosenbrock(x0, x1);
	gsl_rosenbrock_no_jacobian(x0, x1);
	printf("\n\nExercise B.3 - Himmelblaus gsl \n");
	gsl_himmelb(x0, x1);
	gsl_himmelb_no_jacobian(x0, x1);

	printf("\nBy looking at the information above all the methods seem to find the root.\nAlso generally we see that not including the Jacobian results in fewer calls.\nLastly, the GSL routines seem to be better optimized.\n\n");

	//freeing
	//Exercise 1
	vector_free(x_A2); vector_free(fx_A2); matrix_free(j_A2);
	vector_free(x_A3); vector_free(fx_A3); matrix_free(j_A3);
	vector_free(x_A4); vector_free(fx_A4); matrix_free(j_A4);

	//Exercise 2
	vector_free(x_B2); vector_free(fx_B2);
	vector_free(x_B3); vector_free(fx_B3);
	vector_free(x_B4); vector_free(fx_B4);
}
