#include<gsl/gsl_matrix.h>
#ifndef HAVE_OPERATIONS_H
#define HAVE_OPERATIONS_H
void matrix_print(gsl_matrix * A);
void vector_print(gsl_vector * v);

#endif
