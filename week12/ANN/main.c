#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include"ann.h"

//"af" for activation function
double af_1(double x){
	return x*exp(-x*x);}

double af_1_derivative(double x){
	return exp(-x*x)*(1-2*x*x);}

double af_1_antiderivative(double x){
	return -exp(-x*x)/2;}

double af_2(double x){
	return exp(-x*x);}

double af_2_derivative(double x){
	return -2*x*exp(-x*x);}

double af_2_antiderivative(double x){
	return sqrt(M_PI)*erf(x)*1/2;} //1/2*sqrt(M_PI)*erf(x)

double function_to_fit1(double x){
	return sin(x);}

double function_to_fit1_derivative(double x){
	return cos(x);}

double function_to_fit1_antiderivative(double x){
	return -cos(x);}

double function_to_fit2(double x){
	return cos(5*x-1)*exp(-x*x);}


int main(){

	int n = 5;					//dimension of hidden layer
	ann * network1 = ann_alloc(n, af_1);		//creating neural network by specifying neuron activation function
	ann * network2 = ann_alloc(n, af_2);
	double a = -M_PI/2, b = M_PI/2;				//domain of function to fit
	int nx = 100;					//200 training points

	gsl_vector * vx1 = gsl_vector_alloc(nx);
	gsl_vector * vy1 = gsl_vector_alloc(nx);
	gsl_vector * vdy1 = gsl_vector_alloc(nx);
	gsl_vector * vY1 = gsl_vector_alloc(nx);

	//creating x and y datapoints that the neural net has to fit
	for(int i = 0; i < nx; i++){
		double x1 = a + (b-a)*i/(nx-1);
		double y1 = function_to_fit1(x1);
		double dy1 = function_to_fit1_derivative(x1);
		double Y1 = function_to_fit1_antiderivative(x1);
		gsl_vector_set(vx1, i, x1);
		gsl_vector_set(vy1, i, y1);
		gsl_vector_set(vdy1, i, dy1);
		gsl_vector_set(vY1, i, Y1);

	}

	//data contain {ai, bi, wi} and is a vector of size 3n!
	//the ai of the individual neurons are intialized to be different
	//but within domain of seeked function too capture different behaviors!
	//the weights and bi are just initialized to be the same
	for(int i = 0; i < network1->n; i++){
		//network 1 initialization
		gsl_vector_set(network1->data, 3*i, a + (b-a)*i/(network1->n-1));
		gsl_vector_set(network1->data, 3*i+1, 1);
		gsl_vector_set(network1->data, 3*i+2, 1);
		//network 2 initialization
		gsl_vector_set(network2->data, 3*i, a + (b-a)*i/(network2->n-1));
		gsl_vector_set(network2->data, 3*i+1, 1);
		gsl_vector_set(network2->data, 3*i+2, 1);
	}

	//EXERCISE A and B
	int nsteps1 = ann_train(network1, vx1, vy1);
	int nsteps2 = ann_train(network2, vx1, vy1);
	printf("EXERCISE A and B (see figure):\n\n");
	printf("Network with activation function x*exp(-x*x):\n\n");
	printf("Number of steps to fit: %i\n", nsteps1);
	printf("Final values of hidden neurons:\n");
	printf("    #ai      #bi     #wi\n");
	for(int i = 0; i < network1->n; i++){
		double a = gsl_vector_get(network1->data, 3*i);
		double b = gsl_vector_get(network1->data, 3*i + 1);
		double w = gsl_vector_get(network1->data, 3*i + 2);
		printf("%7.3g  %7.3g  %7.3g\n", a, b, w);
	}

	printf("\n\n");

	printf("Network with activation function exp(-x*x):\n\n");
	printf("Number of steps to fit: %i\n", nsteps2);
	printf("Final values of hidden neurons:\n");
	printf("    #ai      #bi     #wi\n");
	for(int i = 0; i < network2->n; i++){
		double a = gsl_vector_get(network2->data, 3*i);
		double b = gsl_vector_get(network2->data, 3*i + 1);
		double w = gsl_vector_get(network2->data, 3*i + 2);
		printf("%7.3g  %7.3g  %7.3g\n", a, b, w);
	}

	printf("\n\n");

	printf("As seen from the figures and the table the ANN is able to fit the function very well.\nHowever the derivative and antiderivative approximations are not spot on!");
	printf("\nThe tables of the ANNs' predictions that were used to create the figure are presented below.\n'f' is used for the prediction made by the NN and 'y' the actual value.\nThe index '1' and '2' are used to indicate which network (activation function) was used.");
	printf("\n\n");

	printf("   #x1     #y1      #dy1      #Y1\n");
	for(int i = 0; i < vx1->size; i++){
		double x1 = gsl_vector_get(vx1, i);
		double y1 = gsl_vector_get(vy1, i);
		double dy1 = gsl_vector_get(vdy1, i);
		double Y1 = gsl_vector_get(vY1, i);
		printf("%8.4g %8.4g %8.4g %8.4g\n", x1, y1, dy1, Y1);
	}

	printf("\n\n");

	double dz = 0.1;

	//network 1 (activation function x*exp(-x*x))
	printf("   #x1     #f1      #df1      #F1\n");
	for(double z = a; z <= b; z += dz){
		double f1 = ann_feed_forward(network1, z);
		double df1 = ann_feed_forward_derivative(network1, af_1_derivative, z);
		double F1 = ann_feed_forward_antiderivative(network1, af_1_antiderivative, z);
		printf("%8.4g %8.4g %8.4g %8.4g\n", z, f1, df1, F1);
	}

	printf("\n\n");

	//network 2 (activation function exp(-x*x))
	printf("   #x2     #f2      #df2      #F2\n");
	for(double z = a; z <= b; z += dz){
		double f2 = ann_feed_forward(network2, z);
		double df2 = ann_feed_forward_derivative(network2, af_2_derivative, z);
		double F2 = ann_feed_forward_antiderivative(network2, af_2_antiderivative, z);
		printf("%8.4g %8.4g %8.4g %8.4g\n", z, f2, df2, F2);
	}



	gsl_vector_free(vx1);
	gsl_vector_free(vy1);
	gsl_vector_free(vdy1);
	gsl_vector_free(vY1);
	ann_free(network1);

return 0;
}
