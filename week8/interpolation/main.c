#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"qspline.h"

double l_interp(int n, double * x, double * y, double z); 					// array pointer "interachangable"
double l_interp_integ(int n, double * x, double * y, double z);



int main(){
	int n = 7;
	double x[n], y[n];
	printf("#x   #sin(x)\n");
	for(int i = 0; i < n; i++){
		x[i] = i;
		y[i] = sin(i);
		printf("%g %g\n", x[i], y[i]);
	}

	printf("\n\n");
	qspline * Q = qspline_alloc(n, x, y);

	//linear slpine
	double z; double dz = 0.1;
	double lz; double l_integ;
	printf("#z #lz #l_integ #sin(z)\n");
	for(z = 0; z <= x[n-1]; z += dz){ 							// n-1 is last element!
		lz = l_interp(n, x, y, z); 							//arrays are refered to as "pointers"
		l_integ = l_interp_integ(n, x, y, z);
		printf("%g %g %g %g\n", z, lz, l_integ, sin(z));
	}
	printf("The interpolation integral of sin(x) from 0 to 6 is: %g\n",l_integ);
	printf("The exact integral of sin(x) from 0 to 6 is: %g\n\n", 0.039839);

	printf("\n\n");

	//quadratic spline
	double qz; double dqz; double iqz;
	printf("#z #qz #dqz #iqz #sin(z)\n");
	for(z = 0; z <= x[n-1]; z += dz){                                                       // n-$
                qz = qspline_evaluate(Q, z);
                dqz = qspline_derivative(Q, z);
                iqz = qspline_integral(Q, z);
                printf("%g %g %g %g %g\n", z, qz, dqz, iqz, sin(z));
	}
	qspline_free(Q);
	printf("\n\n");
	printf("The interpolation integral of sin(x) from 0 to 6 is: %g\n", iqz);
	printf("The exact integral of sin(x) from 0 to 6 is: %g\n\n", 0.039839);
	return 0;

}
