#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

//calculates first- and secondorder updates to y to find the errorestimate at the new y point.
//The second order update is saved in yh and the error in dy.
//The first order is only used to find error!
void rkstep12(void f(double x, gsl_vector * y, gsl_vector* dydx), int n,
	double x, double h, gsl_vector * yx, gsl_vector * yxh, gsl_vector * yh_err) {

	int i;
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);
	gsl_vector* yt = gsl_vector_alloc(n);

	f(x,yx,k0); double k0_i, k12_i, yt_i;		 //finds k0 = f(t0, y0) = dy0/dt0!
	for(i = 0; i < n; i++) {
		yt_i = gsl_vector_get(yx, i);
		k0_i = gsl_vector_get(k0, i);
		gsl_vector_set(yt, i, yt_i+k0_i*h/2);
	}
	f(x+h/2, yt, k12);				//finds k12
	for(i = 0; i < n; i++) {
		yt_i = gsl_vector_get(yx, i);
		k12_i = gsl_vector_get(k12, i);
		gsl_vector_set(yxh, i, yt_i+k12_i*h);
	}
	for(i = 0; i < n; i++) {
		k0_i = gsl_vector_get(k0, i);
		k12_i = gsl_vector_get(k12, i);
		gsl_vector_set(yh_err, i, (k0_i-k12_i)*h/2);
	}

	gsl_vector_free(k0);
	gsl_vector_free(k12);
	gsl_vector_free(yt);

}

//stepper is included as parameter to allow for different steppers to be used!
int driver(void f(double x, gsl_vector * y, gsl_vector * dydx),
	void stepper(void f(double x, gsl_vector * y, gsl_vector * dydx), int n,  double x, double h, gsl_vector * yx,  gsl_vector * yxh, gsl_vector * err),
	int n, gsl_vector * xlist, gsl_matrix * ylist,
	double b, double h, double acc, double eps, int max){

	gsl_vector * yh = gsl_vector_alloc(n);
	gsl_vector * yh_err = gsl_vector_alloc(n);
	gsl_vector * y = gsl_vector_alloc(n);

	int i, k = 0;
	double x, s, err, normy, tol, a = gsl_vector_get(xlist, 0);

	while(gsl_vector_get(xlist, k) < b) {
		x = gsl_vector_get(xlist, k), gsl_matrix_get_row(y, ylist, k);		//storing the path

		if(x+h > b) h = b-x;

		stepper(f, n, x, h, y, yh, yh_err);			//sets yh and err (found elementwise)!

		s = 0;
		for (i = 0; i < n; i++) {
			s += gsl_vector_get(yh_err, i)* gsl_vector_get(yh_err, i);
		}
		err = sqrt(s);						//total err; see (41)
		s = 0;
		for (i = 0; i < n; i++) {
			s += gsl_vector_get(yh, i)*gsl_vector_get(yh, i);
		}
		normy = sqrt(s);					//see (41)
		tol = (normy*eps+acc)*sqrt(h/(b-a));			//see (41)

		if (err < tol) {
			k++;
			if(k > max-1) return -k;
			gsl_vector_set(xlist, k, x+h);
			for (i = 0; i < n; i++) gsl_matrix_set(ylist, k, i, gsl_vector_get(yh, i));
		}
		if(err > 0) h *= pow(tol/err,0.25)*0.95;
		else h *= 2;
	}
	gsl_vector_free(yh);
	gsl_vector_free(yh_err);
	gsl_vector_free(y);
	return k+1;

}

