#define _ISOC99_SOURCE
/* macro enables features from ISO C99 and therefore only relevant when using earlier
language version */
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

//the variable is not part of params! In this case we have no params!
double integrand (double x, void* params) {
	double f = log(x)/sqrt(x);
	return f;
}

double lnxbysqrtx(double x){
	int limspace = 100;
	gsl_integration_workspace * w = gsl_integration_workspace_alloc(limspace);
	gsl_function F;
	F.function = integrand;
	F.params = (void *)&x;
	double result, error;
	double acc = 1e-8, eps = 1e-8;
	int flags = gsl_integration_qags(&F,x, x+1, acc, eps, limspace, w, &result, &error);
	if(flags!=GSL_SUCCESS)return NAN;
	gsl_integration_workspace_free(w);
	return result;
}

