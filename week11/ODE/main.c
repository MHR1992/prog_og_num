#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void rkstep12(void f(double x, gsl_vector* y, gsl_vector* dydx), int n, double x, double h, gsl_vector* yx,  gsl_vector* yxh, gsl_vector* err) ;

int driver(void f(double x, gsl_vector * y, gsl_vector * dydx),
	void stepper(void f(double x, gsl_vector * y, gsl_vector * dydx), int n, double x, double h, gsl_vector * yx,  gsl_vector * yxh, gsl_vector * err),
	int n, gsl_vector * xlist, gsl_matrix * ylist,
	double b, double h, double acc, double eps, int max);

void sincos(double x, gsl_vector * y, gsl_vector * dydx) {
	double y0 = gsl_vector_get(y, 0);
	double y1 = gsl_vector_get(y, 1);
	gsl_vector_set(dydx, 0, y1);
	gsl_vector_set(dydx, 1, -1*y0);
}

void relativistic_orbit(double t, gsl_vector * y, gsl_vector * dydx){
	double eps = 0.01;				//epsilon from progsprog exercise that made motion rel
	double y0 = gsl_vector_get(y, 0);
	double y1 = gsl_vector_get(y, 1);
	gsl_vector_set(dydx, 0, y1);
	gsl_vector_set(dydx, 1, 1 - y0 + eps*y0*y0);
}


int main() {
	int n = 2, t_steps = 100;

	printf("Numerical solution to ODE system y[1]' = y[0]\n");
	gsl_vector * t = gsl_vector_alloc(t_steps);
	gsl_matrix * y = gsl_matrix_alloc(t_steps,n);		//for storing the path - only y values
	double a = 0, b = 3*M_PI, h = 0.5, acc = 0.1, eps = 0.1;

	gsl_vector_set(t, 0, a);
	gsl_matrix_set(y, 0, 0, 0);
	gsl_matrix_set(y, 0, 1, 1);

	int k = driver(sincos, rkstep12, n, t, y, b, h, acc, eps, t_steps);
	double t_i, y_i1, y_i2;
	for(int i = 0; i < k; i++){
		t_i = gsl_vector_get(t, i);
		y_i1 = gsl_matrix_get(y, i, 0);
		y_i2 = gsl_matrix_get(y, i, 1);
		printf("%8.5g\t %8.5g\t %8.5g\t %8.5g\t %8.5g\n", t_i, y_i1, sin(t_i), y_i2, cos(t_i));
	}
	printf("\n\n");

}

