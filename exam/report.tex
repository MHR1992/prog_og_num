\documentclass[english, a4paper, amsmath, amssymb]{article}
\usepackage{graphicx}
\usepackage{svg} % able to include svg
\usepackage{amsmath} % need this to use svg package
\usepackage{listings} % can add source code ir rigth format

\begin{document}
\title{Exam - Inverse Rayleigh quotient iteration}
\date{\today}
\author{Michael Rüdiger}
\maketitle

\section{Introduction}
The purpose of the algorithm presented below is to find the eigenvalue
(and eigenvector) closest to some given value s, which the user supplies.
In this aspect it should be noted that this particular algorithm often is
used when one already knows a value s that is close to some sought eigenvalue.

\section{Theory}
The algorithm that finds this eigenvalue is called the Rayleigh quotient
iteration. More specifically it is an inverse power iteration method that
is shifted by some value $\mathrm{s}$(times the identity matrix). Generally we have that
iteration like in \ref{eq:IPM} converges towards a solution giving
the lowest eigenvalue (see \ref{eq:RQ}) for how to find the eigenvalue).
\begin{equation}
	x_{i+1} = A^{-1}x_i
\label{eq:IPM}
\end{equation} 

\ref{eq:IPM} In the shifted inverse power method (the algorithm presented below)
the iteration instead looks like:

\begin{equation}
	x_{i+1} = (A-sI)^{-1}x_i
\label{eq:IPMS}
\end{equation}

which eigenvalue converges towards the one closest to the guess, $\mathrm{s}$.
This can be solved by not inverting the matrix but instead use
QR-decomposition to solve:

\begin{equation}
	(A-sI)x_{i+1} = x_i
\end{equation}

where $\mathrm{(A-sI)}$ would be the matrix that needs to be decomposed. The eigenvalue
of some $\mathrm{x_i}$ is estimated through the Rayleigh quotient:

\begin{equation}
	\lambda[x_i] = \dfrac{x_i^TAx_i}{x_i^Tx_i}
\label{eq:RQ}
\end{equation}

To get a faster convergence $\mathrm{s}$ can be updated by the Rayleigh quotient. 

\section{Code and concluding comments}
Below the implementation of the algorithm can be seen. The code has been stripped of all prints
and comments (to see the full code see powermethods.c). In correpondence with the theory section above the algorithm
takes both the matrix $\mathrm{A}$ and the guess of an eigenvalue $\mathrm{s}$. It should be noted that
the function also takes $\mathrm{int raleigh_switch}$ which determines if $\mathrm{inverse_power_method}$
updates $\mathrm{s}$ (using the Rayleigh quotient). Also the while loop is terminated when an eigenvalue has been found
- when the relative and absoulute difference between $\mathrm{Ax_i}$ and $\mathrm{\lambda[x_i]x_i}$ are within predetermined
tolerances. 

\begin{lstlisting}

void inverse_power_method(gsl_matrix * A, double s_init, 
	gsl_vector * x_init, int rayleigh_switch, double abs, double rel){

	gsl_vector * x_i_new = gsl_vector_calloc(A->size2);
	gsl_matrix * A_s1 = gsl_matrix_alloc(A->size1, A->size2);
	gsl_matrix * s1 = gsl_matrix_calloc(A->size1, A->size2);
	gsl_matrix * R = gsl_matrix_calloc(A->size2, A->size2);

	int i = 0;
	while (1) {
		i++;
		gsl_matrix_memcpy(A_s1, A);
		gsl_matrix_set_identity(s1);
		gsl_matrix_scale(s1, s_init);
		gsl_matrix_sub(A_s1, s1);

		if(rayleigh_switch == 1 && i%2 == 0){
			s_init = rayleigh_quotient(A, x_init);
		}

		qr_gs_decomp(A_s1, R);
		qr_gs_solve(x_i_new, A_s1, R, x_init);

		gsl_vector * Ax_i_new = gsl_vector_calloc(A->size1);
		gsl_vector * sx_i_new = gsl_vector_calloc(A->size1);
		gsl_blas_dgemv(CblasNoTrans, 1, A, x_i_new, 0, Ax_i_new);
		gsl_blas_daxpy(rayleigh_quotient(A, x_init),
			 x_i_new, sx_i_new);
		gsl_vector_memcpy(x_init, x_i_new);

		if(check_gsl_vectors_equal(Ax_i_new, sx_i_new, abs, rel) 
			== 1){
			double x_final_nrm = gsl_blas_dnrm2(x_init);
			gsl_vector_scale(x_init, 1/x_final_nrm);

			gsl_vector_free(Ax_i_new);
			gsl_vector_free(sx_i_new);
			break;
		}

		gsl_vector_free(Ax_i_new);
		gsl_vector_free(sx_i_new);

		if(i > 1000){
			break;
		}
	}
	gsl_vector_free(x_i_new);
	gsl_matrix_free(A_s1);
	gsl_matrix_free(s1);
	gsl_matrix_free(R);
}
\end{lstlisting} 

Generally, it was found that the number of iterations decreases much
by updating s. However, it was also seen that using the update (setting $\mathrm{rayleigh_switch = 1}$)
made the convergence to a different eigenvalue (from that nearest to the value of s passed to the function) more likely.  

\end{document}
