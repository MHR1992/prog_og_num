#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<pthread.h>

struct params {int n, N_in; unsigned int seed;};

void * darts_in_counter(void* params){
        struct params * p = (struct params *) params;
        int n = p->n;
        int N_in = 0;
        for(int i = 0; i < n; i++){
                double x = (double) rand_r(&(p->seed))/RAND_MAX;
                double y = (double) rand_r(&(p->seed))/RAND_MAX;
                if(x*x + y*y < 1) N_in++;
        }
        p->N_in = N_in;
        return NULL;
}


int main() {

	int n = 1e6;

	//make params
	struct params p1, p2, p3;
	p1.n = n/3; p1.seed = 3;
	p2.n = n/3; p2.seed = 4;
	p3.n = n/3; p3.seed = 5;

	#pragma omp parallel sections
	{

		#pragma omp section
		{
			darts_in_counter((void *) &p1);
		}
		#pragma omp section
		{
			darts_in_counter((void *) &p2);
		}
		#pragma omp section
		{
			darts_in_counter((void *) &p3);
		}
	}

	double pi = (p1.N_in + p2.N_in + p3.N_in)/(p1.n + p2.n + p3.n);

	printf("pi is found to be %g using OpenMP with N = %i:", pi, n); 
return 0;
}


