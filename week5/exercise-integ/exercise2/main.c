#include<gsl/gsl_integration.h>
#include<stdio.h>
#include<math.h>

//for Exercise 2a
double norm_int (double x, void * params) {
	double a = *(double *) params;
	double nrm = 2*exp(-a*x*x);
	return nrm;
}

//for Exercise 2b
double H_expect ( double x, void *params) {
	double a = *(double *) params;
	double H_expect = 2*(-a*a*x*x/2 + a/2 + x*x/2)*exp(-a*x*x);
	return H_expect;
}

int main() {
	gsl_integration_workspace * w = gsl_integration_workspace_alloc(1000);
	gsl_function F;
	printf("#alpha   #Energy");

	//as we have to see a minimum at a = 1 of E = 0.5 we loop over a
	for (int i = 1; i < 200; i++) {

		double a = i/100.0;
		F.params = &a;
		double epsrel = 1e-6, epsabs = 1e-6;

		//Exercise 2 (hint a)
		double nrm, nrm_err;
		F.function = &norm_int;
		gsl_integration_qagiu(&F, 0, epsabs, epsrel, 1000, w, &nrm, &nrm_err);

		//Exercise 2 (hint b)
		double H_os, H_os_err;
		F.function = &H_expect;
		gsl_integration_qagiu(&F, 0, epsabs, epsrel, 1000, w, &H_os, &H_os_err);

		//Exercise 2 result
		double E = H_os/nrm;

		printf("%g    %g\n", a, E);
	}
	gsl_integration_workspace_free(w);
	return 0;
}

