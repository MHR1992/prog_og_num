#define _ISOC99_SOURCE
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdio.h>
#include<gsl/gsl_errno.h>

struct nx {int n; double x;};

double integrand (double t, void *params){
	struct nx p = *(struct nx *) params;
	int n =p.n;
	double x=p.x;
	double f = cos(n*t-x*sin(t));
	return f;
}

/* 'params' is a void pointer - that means a pointer to any datatype!
'(...)' still means that we cast from one data type to another. Left side: define struct p.
Right side: '(struct nx *)' casts 'params' to a pointer that points to a 'struct nx'. The
'*' in front then dereferences it, so that p gives the value at the location and not the adress.  */

double besseljn(int n, double x){
	int limit=100;
	struct nx params;
	params.n = n;
	params.x = x;

/* number of double precision floating point space allocated to workspace: 100 gives 6400 bits
why not write 'params.n' and 'params.x'? */

	gsl_integration_workspace *w;
	w = gsl_integration_workspace_alloc(limit);
	gsl_function F;
	F.function = integrand;
	F.params = (void*)&params;


/* Creation of pointer 'w' to the gsl workspace (a struct).
This workspace handles the memory for the subinterval ranges, results and errorestimates.
we make 'gsl_integration_workspace_alloc' a pointer as w is one
So a struct is a userdefined data type. The definition of F therefore functions as if it had a 'type' prefix.
As gsl_function is also a struct and F now has this struct '.' can be used
The address of params is cast to be a pointer of void - any type */


	double result, error, acc = 1e-8, eps = 1e-8;
	int flags = gsl_integration_qags (&F, 0, M_PI, acc, eps, limit, w, &result, &error);
/*Integration from 0 to pi. Within the absolute error limit acc and relative error limit 'eps'. 'Limit' is the max
amount of subintervals and must not exceed allocated size of workspace. The functions result is found at '&result'
with absolute error at '&error'.*/
	if(flags!=GSL_SUCCESS) return NAN;
	gsl_integration_workspace_free(w);
	return result;
}
