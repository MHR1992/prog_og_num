void print_gsl_matrix(gsl_matrix * M);
void set_gsl_matrix_random(gsl_matrix * A);
void set_gsl_vector_random(gsl_vector * v);
int check_gsl_vectors_equal(gsl_vector * x, gsl_vector * y, double abs, double rel);

