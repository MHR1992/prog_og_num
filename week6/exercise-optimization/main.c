#include<gsl/gsl_multimin.h>
#include<assert.h>
#define THEO_MOD(t) A*exp(-(t)/T)+B


struct decay_data {
	int n;
	double * time, * signal, * err;};

double deviation_between_model_and_experiment (const gsl_vector * x, void * params) {
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	struct decay_data * p = (struct decay_data *) params;
	int     n = p->n;
	double * time = p->time;
	double * signal = p->signal;
	double * err = p->err;
	double sum = 0;
	for(int i = 0; i < n; i++) sum += pow((THEO_MOD(time[i]) - signal[i]) /err[i] ,2);
	return sum;
}

int main(){

	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double signal[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double err[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};

	int n = sizeof(t)/sizeof(t[0]);
	assert(n == sizeof(signal)/sizeof(signal[0]));
	assert(n == sizeof(err)/sizeof(err[0]));
	fprintf(stderr,"number of experimental points: %i\n", n);

	printf("# t[i], signal[i], err[i]\n");
	for(int i = 0; i < n; i++) printf("%g %g %g\n", t[i], signal[i], err[i]);
	printf("\n\n");

	struct decay_data params;
	params.n = n;
	params.time = t;
	params.signal = signal;
	params.err = err;

	gsl_multimin_function F;
	F.f = deviation_between_model_and_experiment;
	F.n = 3;
	F.params = (void *) &params;

	gsl_vector * start = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, 3);
	gsl_vector_set(start, 1, 2);
	gsl_vector_set(start, 2, 0);

	gsl_vector * step = gsl_vector_alloc(F.n);
	gsl_vector_set(step, 0, 2);
	gsl_vector_set(step, 1, 2);
	gsl_vector_set(step, 2, 2);

	gsl_multimin_fminimizer * state;
	state = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, F.n);
	gsl_multimin_fminimizer_set(state, &F, start, step);

	/* We start by printing out the values for time, signal and err. We pass the arra$
        data to the struct params and further on to the function F! We set the start and $
        We allocate space for the minimizer and initialize ('set') the minimizer state to$
        function 'F' with start value 'start' and first trial step 'step'.
        Here alloc takes a type just like last time nmsimplex2 is a '_type' however!*/

	int iter = 0, status;

	do{
		iter++;
		int iteration_status = gsl_multimin_fminimizer_iterate(state);
		if(iteration_status != 0){
			fprintf(stderr, "unable to improve\n");
			break;
			}

		double abs_tol = 0.01;
		status = gsl_multimin_test_size(state->size, abs_tol);
		if( status == GSL_SUCCESS ) fprintf(stderr, "converged\n");
		fprintf(stderr,"iter = %3i  ", iter);
		fprintf(stderr,"A = %.3f  ", gsl_vector_get(state->x, 0));
		fprintf(stderr,"T = %.3f  ", gsl_vector_get(state->x, 1));
		fprintf(stderr,"B = %.3f  ", gsl_vector_get(state->x, 2));
		fprintf(stderr,"size = %.3f  ", state->size);
		fprintf(stderr,"fval/n = %.3f  ", state->fval/n);
		fprintf(stderr,"\n");

	} while(status == GSL_CONTINUE && iter < 100);

	double A = gsl_vector_get(state->x, 0);
	double T = gsl_vector_get(state->x, 1);
	double B = gsl_vector_get(state->x, 2);

	printf("# t, A*exp(-t/T)+B\n");
	double dt = (t[n-1]-t[0])/50;
	for(double ti = t[0]; ti < t[n-1]+dt; ti += dt) printf("%g %g\n",ti, THEO_MOD(ti));

	gsl_multimin_fminimizer_free(state);
	gsl_vector_free(start);
	gsl_vector_free(step);

	/* We iterate over the ongoing solutions and 'test_size' checks if the found minimum is l$
	than the absolute tolerance 'abs_tol'. Having found the minimum we get the parameter valu$
	Lstly we free the workspace and vector from the memory!*/


	return 0;
}
