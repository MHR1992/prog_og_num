
#include<stdio.h>
#include<unistd.h> /* short comman-line options */
#include<getopt.h> /* long and short comman-line options */
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int exercise_1(double x, const double y[], double yprime[], void * params){
	yprime[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int orbital_equation(double phi, const double y[], double yprime[], void *params){
	double epsilon = *(double *) params;
	yprime[0] = y[1];
	yprime[1] = 1 - y[0] + epsilon * y[0] * y[0];
	return GSL_SUCCESS;
}
/* Converting to first order system gives the relations above - if forgotten do it again.*/


int main (int argc, char **argv) {
	double epsilon = 0.5;
	double uprime = -0.5;

	//here we read parameters from long command-line options
	while (1){
		struct option long_options[] = {
			{"epsilon", required_argument, NULL, 'e'},
			{"uprime" , required_argument, NULL, 'p'},
			{0, 0, 0, 0}
		};

		int opt = getopt_long (argc, argv, "e:p:", long_options, NULL);
		if( opt == -1 ) break;
		switch (opt) {
			case 'e': epsilon = atof (optarg); break;
			case 'p': uprime  = atof (optarg); break;
			default:
				fprintf(stderr, "Usage: %s --epsilon epsilon --uprime uprime\n", argv[0]);
				exit (EXIT_FAILURE);
		}
	}

/* The getopt function returns the option character for the next command line option.
When getopt is '-1' it indicates that no more options are present. When the option is followed
by a colon it indicates that it requires an argument with it. 'atof': string to double if it contains a value.
'optarg': variable containing the current option argument. Default: thing that happens if none of the cases match.
This makes us able to change epsilon and uprime in the makefile (as input to the program when it runs).*/

	//Exercise 2:
	gsl_odeiv2_system orbit;
	orbit.function = orbital_equation;
	orbit.jacobian = NULL;
	orbit.dimension = 2;
	orbit.params = (void *) &epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max = 39.5 * M_PI;
	double delta_phi = 0.05;

	gsl_odeiv2_driver * driver =
		gsl_odeiv2_driver_alloc_y_new(&orbit, gsl_odeiv2_step_rkf45, hstart, epsabs, epsrel);

	double t = 0, y[2] = {1, uprime};
	for (double phi = 0; phi < phi_max; phi += delta_phi) {
		int status = gsl_odeiv2_driver_apply (driver, &t, phi, y);
		printf ("%g %g\n", phi, y[0]);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
		}

	gsl_odeiv2_driver_free (driver);

/* 'hstart': initial step size
'rk8pd': just a type
'apply': udvikler systemet fra t1= &t til t2= phi. Virker lidt forkert. Burde det ikke være phi til phi+delta_phi?
y should contain a vector of the dependent variable at t1.*/

	//Exercise 1 (repeating the same)
	gsl_odeiv2_system logfun;
	logfun.function = exercise_1;
	logfun.jacobian = NULL;
	logfun.dimension = 1;
	gsl_odeiv2_driver * driver2 =
		gsl_odeiv2_driver_alloc_y_new(&logfun, gsl_odeiv2_step_rk4, hstart, epsabs, epsrel);
	double y2[1] = {0.5};
	double t2 = 0;
	printf("\n\n");
	for(double x = 0; x < 3; x += 0.01){
		int status = gsl_odeiv2_driver_apply(driver2, &t2, x, y2);
		printf("%g %g\n", x, y2[0]);
			if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
	}
	 gsl_odeiv2_driver_free (driver2);
/*Doing the same for first exercise.*/
return EXIT_SUCCESS;
}
