#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mv_operations.h"
#include<assert.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"


//vector operations
vector * vector_alloc(int n){
	vector * v = (vector *) malloc(sizeof(vector));
	v->size = n;
	v->data = (double *) malloc(n*sizeof(double));
	return v;
}

void vector_free(vector * v){free(v->data); free(v);}

double vector_get(vector * v, int i){
	assert(i >= 0);
	assert(i < v->size);
	return v->data[i];
}

void vector_set(vector * v, int i, double x){
	assert(i >= 0);
	assert(i < v->size);
	v->data[i] = x;
}


void vector_set2(vector * v, int i, double x){
	assert(i >= 0);
	assert(i < v->size);
	v->data[i] = x;
}


void vector_print(vector * v){
	for(int i = 0; i < v->size; i++) printf(FMT, vector_get(v, i));
	printf("\n");
}
double vector_dot(vector * v1, vector * v2){
	assert(v1->size == v2->size);
	double sum = 0;
	for(int i = 0; i < v1->size; i++) sum += vector_get(v1, i)*vector_get(v2, i);
	return sum;
}

double vector_modulus(vector * v){
	double mod = sqrt(vector_dot(v, v));
	return mod;
}

void vector_scale(vector * v, double x){
	for(int i = 0; i < v->size; i++) v->data[i] *= x;
}

void vector_add(double a, vector * v1, double b, vector * v2){
	assert(v1->size == v2->size);
	for(int i = 0; i < v1->size; i++){
		double v1_i = vector_get(v1, i);
		double v2_i = vector_get(v2, i);
		vector_set(v1, i, a*v1_i+b*v2_i);
	}
}

void vector_sub(double a, vector * v1, double b, vector * v2){
	assert(v1->size == v2->size);
	for(int i = 0; i < v1->size; i++){
		double v1_i = vector_get(v1, i);
		double v2_i = vector_get(v2, i);
		vector_set(v1, i, a*v1_i-b*v2_i);
	}
}

void vector_random(vector * v){
	for(int i = 0; i < v->size; i++) vector_set(v, i, RND);
}

void vector_cpy(vector * v1, vector * v2){
	assert(v1->size == v2->size);
	for(int i = 0; i < v1->size; i++) v1->data[i] = v2->data[i];
}

void vector_normalize(vector * v){
	vector_scale(v, 1/vector_modulus(v));
}

//matrix vector operations
void matrix_vector_dot(vector * x, matrix * A, const char T, vector * v){
	if(T == 'T') A = matrix_transpose(A);
	assert(A->size2 == v->size);
	assert(x->size == A->size1);
	for(int i = 0; i < A->size1; i++){
		double sum = 0;
		for(int k = 0; k < A->size2; k++){
			sum += matrix_get(A, i, k)*vector_get(v,k);
		}
		vector_set(x, i, sum);
	}
}


void matrix_vector_dot2(vector * res, double a, matrix * A, const char T, vector * v){
	if(T == 'T') A = matrix_transpose(A);
	assert(A->size2 == v->size);
	assert(res->size == A->size1);
	for(int i = 0; i < A->size1; i++){
		double sum = 0;
		for(int k = 0; k < A->size2; k++){
			sum += matrix_get(A, i, k)*vector_get(v,k)*a;
		}
		vector_set(res, i, sum);
	}
}


void matrix_vector_add_dot(vector * res, double b, double a,  matrix * A, const char T, vector * v){
	if(T == 'T') A = matrix_transpose(A);
	assert(A->size2 == v->size);
	assert(res->size == A->size1);
	for(int i = 0; i < A->size1; i++){
		double sum = 0;
		for(int k = 0; k < A->size2; k++){
			sum += matrix_get(A, i, k)*vector_get(v,k)*a;
		}
		vector_set(res, i, sum+b*vector_get(res, i));
	}
}




//matrix operations
matrix * matrix_alloc(int n, int m){
	matrix * A = (matrix *) malloc(sizeof(matrix));
	A->size1 = n, A->size2 = m;
	A->data = (double *) malloc(n*n*sizeof(double));
	return A;
}

void matrix_free(matrix * A){ free((*A).data); free(A);}			//once again... the order is important!

inline void matrix_set(matrix * A, int i, int j, double x){				//Dmitri uses inline
	A->data[i+j*(*A).size1] = x;
}

inline double matrix_get(matrix * A, int i, int j){
	return A->data[i+j*(*A).size1];
}

void matrix_print(matrix * A){
	for(int r = 0; r < A->size1; r++) {
		for(int c = 0; c < A->size2; c++) printf(FMT, matrix_get(A, r, c));
		printf("\n");
	}
}

void matrix_random(matrix * A){
	for(int i = 0; i < A->size1; i++) for(int j = 0; j < A->size2; j++)
		matrix_set(A, i, j, RND);
}

matrix * matrix_cpy(matrix * A){
	matrix * B = matrix_alloc(A->size1, A->size2);
	for(int i = 0; i < A->size1*A->size2; i++) B->data[i] = A->data[i];
	return B;
}

void matrix_cpy2(matrix * A, matrix * B){
	assert(A->size1 == B->size1);
	assert(A->size2 == B->size2);
	for(int i = 0; i < A->size1; i++){
		for (int j = 0; j < A->size2; j++){
			matrix_set(A, i, j, matrix_get(B, i, j));
		}
	}

}

vector * matrix_get_column(matrix * A, int j){						//remember to free in script!
	assert(j < A->size2);
	vector * v = vector_alloc(A->size1);
	for(int i = 0; i < A->size1; i++){
		vector_set(v, i, matrix_get(A, i, j));
	}
	return v;
	/*vector v = {.size = A->size1, .data = A->data + j*A->size1};			//result in core dump
	vector * vp = &v;
	return vp;*/
}

void matrix_get_column2(matrix * A, int j, vector * v){
	assert(j < A->size2);
	for(int i = 0; i < A->size1; i++){
		vector_set(v, i, matrix_get(A, i, j));
	}
}

void matrix_get_row(vector * v, matrix * A, int i){
	assert(i < A->size1);
	for(int j = 0; j < A->size2; j++){
		vector_set2(v, j, matrix_get(A, i, j));
	}
}

void matrix_set_column(matrix * A, int j, vector * v){
	assert(j < A->size2 && A->size1 == v->size);
	for(int i = 0; i < A->size1; i++) matrix_set(A, i, j, vector_get(v, i));
}

matrix * matrix_transpose(matrix * A){
	matrix * B = matrix_alloc(A->size2, A->size1);
	for(int i = 0; i < A->size1; i++) for(int j = 0; j < A->size2; j++){
		matrix_set(B, j, i, matrix_get(A, i, j));
	}
	return B;
	/*A = B;
	matrix_free(B);*/
}

void matrix_mm(matrix * AB, matrix * A, matrix * B){
	assert(A->size2 == B->size1);
	assert(A->size1 == AB->size1 && B->size2 == AB->size2);
	for (int i = 0; i < AB->size1; i ++) for(int j = 0; j < AB->size2; j++){
		double sum = 0;
		for(int k = 0; k < A->size2; k++){
			sum += matrix_get(A, i, k)*matrix_get(B, k, j);
		}
		matrix_set(AB, i, j, sum);
	}
}

void matrix_set_identity(matrix * A){
	assert(A->size1 == A->size2);
	for(int i = 0; i < A->size1; i++){
		matrix_set(A, i, i, 1);
		for(int j = i+1; j < A->size1; j++){
			matrix_set(A, i, j, 0);
			matrix_set(A, j, i, 0);
		}
	}

}

void matrix_add_outprod(matrix * B, double a, vector * v1, vector * v2){
	assert(v1->size == v2->size);
	for(int i = 0; i < v1->size; i++){
		for(int j = 0; j < v1->size; j++){
			matrix_set(B, i, j, matrix_get(B, i, j)+a*vector_get(v1, i)*vector_get(v2, j));
		}
	}
}
