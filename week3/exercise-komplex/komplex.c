#include<stdio.h>
#include"komplex.h"
#include<math.h>

void komplex_print(char *s, komplex a){
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

void komplex_set(komplex* z, double x, double y){
        (*z).re = x;
        (*z).im = y;
}

//We use the adress/pointer to z instead of the value!

komplex komplex_new(double x, double y){
	komplex z = {x, y};
	return z;
}

komplex komplex_add(komplex a, komplex b){
	komplex result = {a.re + b.re, a.im + b.im};
	return result;
}

komplex komplex_sub(komplex a, komplex b){
	komplex result = {a.re - b.re, a.im - b.im};
	return result;
}

int komplex_equal(komplex a, komplex b, double acc, double eps){
	komplex diff = komplex_sub(a, b);
	komplex sum = komplex_add(a, b);
	if(fabs(diff.re) < acc && fabs(diff.im) < acc) return 1;
	if(fabs(diff.re/sum.re) < eps/2 && fabs(diff.im/sum.im) < eps/2) return 1;
	return 0;
}


komplex komplex_mul(komplex a, komplex b){
	komplex result = {a.re*b.re-a.im*b.im, a.re*b.im+a.im*b.re};
	return result;
}


komplex komplex_div(komplex a, komplex b){
	komplex result = {(a.re*b.re+a.im*b.im)/(b.re*b.re+b.im*b.im),
		(a.im*b.re-a.re*b.im)/(b.re*b.re+b.im*b.im)};
	return result;
}

komplex komplex_conjugate(komplex a){
        komplex result = {a.re, -a.im};
        return result;
}

komplex komplex_exp(komplex a){
        komplex result = {exp(a.re)*cos(a.im), exp(a.re)*sin(a.im)};
        return result;
}

komplex komplex_cos(komplex a){
	komplex result = {cos(a.re)*cosh(a.im), sin(a.re)*sinh(a.im)};
	return result;
}

/* ... */
