#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

void matrix_print(const char * s, const gsl_matrix * m){
	printf("%s", s);
	printf("\n");
	for(int j = 0; j < m->size2; j++){
		for(int i = 0; i < m->size1; i++){
			printf("%8.3g ", gsl_matrix_get(m,i,j));
		}
		printf("\n");
	}
}
/* Print out the string above and the matrix below.
'gsl_matrix': structure containing 6 components - including the two dimensions. */

void vector_print(const char* s, const gsl_vector * v){
	printf("%s", s);
	printf("\n");
	for(int i = 0; i < v->size; i++){
		printf("%8.3g ", gsl_vector_get(v,i));
	}
	printf("\n");
}
/* Printing out the vector.*/

int main(){

	int n = 3;
	gsl_matrix * M = gsl_matrix_calloc(n,n);
	double mat_elem[3][3] = {
	{6.13, 2.90, 5.86}, {8.08, 6.31, 3.89}, {4.36, 1.00, 0.19}};
	double vec_elem[3] = {6.23, 5.37, 2.29};

	for(int i = 0; i < M->size1; i++)
		for(int j = 0; j < M->size2; j++)
			gsl_matrix_set(M, i, j, mat_elem[i][j]);
	gsl_vector * b = gsl_vector_calloc(n);
	for(int i = 0; i < b->size; i++)
		gsl_vector_set(b, i, vec_elem[i]);

	matrix_print("The matrix M is equal to:", M);
	vector_print("The vector b is equal to: ", b);
	gsl_vector * x = gsl_vector_calloc(n);
	gsl_matrix * A = gsl_matrix_calloc(n,n);
	gsl_matrix_memcpy(A, M);
	//gsl_permutation * p = gsl_permutation_alloc(n);
	//int s;
	gsl_vector * tau = gsl_vector_calloc(n);
	gsl_linalg_QR_decomp(M, tau);
	gsl_linalg_QR_solve(M, tau, b, x);
	//gsl_linalg_HH_solve(M, b, x);
	//gsl_linalg_LU_decomp(M, p, &s);
	//gsl_linalg_LU_solve(M, p, b, x);
	vector_print("solution x to Mx=b is:", x);
	gsl_vector * y = gsl_vector_calloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, y);
	vector_print("check that the b is correct with this x:", y);

	gsl_matrix_free(M);
	gsl_matrix_free(A);
	gsl_vector_free(tau);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(y);
	//gsl_permutation_free(p);
return 0;
}

/*Could have used size_t instead of integer but i does not matter here.
'gsl_matrix_calloc': allocation of memory to matrix struct with all elements
equal to 0.
In the for loops we set the matrix and vector values.
'gsl_matrix_memcpy': copies the matrix M into A. This is done as M changes when we decompose and solve the system.
'gsl_linalg_HH_solve': uses householder tranformations to solve the system Ax=b.
'gsl_blas_dgemv': a lvl 2 matrix vector operation - dot product (Basic linear algebra Subprograms)*/
