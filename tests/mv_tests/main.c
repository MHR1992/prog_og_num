#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"mv_operations.h"
#include<assert.h>

int main(){

	int n = 2;
	//create vectors and matrices
	vector * v_mv = vector_alloc(n);
	vector * v_mv2 = vector_alloc(n);

	matrix * m_mv = matrix_alloc(n, n);

	vector_random(v_mv);
	matrix_random(m_mv);
	gsl_vector * v_gsl = gsl_vector_alloc(n);
	gsl_vector * v_gsl2 = gsl_vector_alloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set(v_gsl, i, vector_get(v_mv, i));
	}
	gsl_matrix * m_gsl = gsl_matrix_alloc(n, n);
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++) {
			gsl_matrix_set(m_gsl, i, j, matrix_get(m_mv, i, j));
		}
	}
	printf("v_mv:\n"); vector_print(v_mv);
	printf("\n");
	gsl_vector_fprintf(stdout, v_gsl, "%7.3g");
	printf("m_mv:\n"); matrix_print(m_mv);
	printf("\nm_gsl:");
	gsl_matrix_fprintf(stdout, m_gsl, "%7.3g");


	//tests
	//matrix vector dot
	gsl_blas_dgemv(CblasNoTrans, 1, m_gsl, v_gsl, 0, v_gsl2);
	matrix_vector_dot(v_mv2, m_mv, ' ', v_mv);
	printf("mv matrix vector dot\n");
	vector_print(v_mv2);
	printf("gsl matrix vector dot\n");
	gsl_vector_fprintf(stdout, v_gsl2, "%7.3g");

	//vector modulus
	double mod_gsl = gsl_blas_dnrm2(v_gsl);
	double mod_mv = vector_modulus(v_mv);
	printf("\nModulus methods are equal:\n%d\n ", mod_gsl == mod_mv);

	//vector add
	printf("v_mv:\n"); vector_print(v_mv);
	vector_add(1, v_mv, 1, v_mv);
	gsl_vector_add(v_gsl, v_gsl);
	printf("\nAdd methods are equal:\n%d\n", gsl_vector_get(v_gsl, 1) == vector_get(v_mv, 1));

	//vector dot
	double dot_gsl;
	gsl_blas_ddot(v_gsl, v_gsl, &dot_gsl);
	double dot_mv = vector_dot(v_mv, v_mv);
	printf("\nDot methods are equal:m\n%d\n", dot_gsl == dot_mv); 

	//scale methods
	gsl_vector_scale(v_gsl, 0.5);
	vector_scale(v_mv, 0.5);
	printf("Scale methods are equal: \n%d\n", gsl_vector_get(v_gsl, 1) == vector_get(v_mv, 1));

	//y = ax + y
	gsl_blas_daxpy(-1, v_gsl, v_gsl);
	vector_sub(1, v_mv, 1, v_mv);
	printf("daxpy and sub gives same result: \n%d\n", gsl_vector_get(v_gsl, 1) == vector_get(v_mv, 1));

	//add dot - virker også!
	gsl_blas_dgemv(CblasNoTrans, -1, m_gsl, v_gsl, 1, v_gsl2);
	matrix_vector_add_dot(v_mv2, 1, -1, m_mv, ' ', v_mv);
	vector_print(v_mv2);
	gsl_vector_fprintf(stdout, v_gsl2, "%7.3g");

	//r1 update
	gsl_matrix_set_identity(m_gsl); matrix_set_identity(m_mv);

	printf("v_mv2:\n"); vector_print(v_mv2);
	matrix_add_outprod(m_mv, 1.0, v_mv2, v_mv2);
	printf("\nm_mv:\n");
	matrix_print(m_mv);

	printf("v_gsl2:\n"); gsl_vector_fprintf(stdout, v_gsl2, "%7.3g"); 
	gsl_blas_dger(1.0, v_gsl2, v_gsl2, m_gsl);
	printf("\nm_gsl:\n");
	gsl_matrix_fprintf(stdout, m_gsl, "%7.3g");

	printf("matrix:\n"); matrix_print(m_mv);
	matrix_get_row(v_mv2, m_mv, 1);
	printf("\n"); vector_print(v_mv2);
	printf("%i:", v_mv2->size);

	//vector_modulus
	vector * v_for_normalize = vector_alloc(3);
	vector_random(v_for_normalize);
	printf("v_for_normalize (before):\n"); vector_print(v_for_normalize);
	printf("modulus of v_for_normalize (before):\n%g\n", vector_modulus(v_for_normalize));
	vector_normalize(v_for_normalize);
	printf("v_for_normalize (after):\n"); vector_print(v_for_normalize);
	printf("modulus of v_for_normalize (after):\n%g\n", vector_modulus(v_for_normalize));

return 0;
}
