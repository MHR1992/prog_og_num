#include<math.h>
#include<stdio.h>

void simplex_update(
	int d, double simplex[][d], double * f_values,
	int * hi, int * lo, double * centroid){

	* hi = 0; double highest = f_values[0];
	* lo = 0; double lowest = f_values[0];

	for(int k = 1; k < d+1; k++){
		double next = f_values[k];
		if(next>highest){highest = next; *hi = k;}
		if(next<lowest){lowest = next; *lo = k;}
	}

	for(int i = 0; i < d; i++){
		double sum = 0;
		for(int k = 0; k < d + 1; k++){
			if(k != * hi) sum += simplex[k][i];
		}
		centroid[i] = sum/d;
	}
}

void simplex_init(int d, double (* fun)(double *), double simplex[][d],
	double * f_values, int * hi, int * lo, double * centroid){

	for(int k = 0; k < d + 1; k++){
		f_values[k] = fun(simplex[k]);
	}
	simplex_update(d, simplex, f_values, hi, lo, centroid);
}

void reflection(int dim, double * highest, double * centroid, double * reflected){
	for(int i = 0; i < dim; i++){
		reflected[i] = 2*centroid[i]-highest[i];
	}
}

void expansion(int dim, double * highest , double * centroid, double * expanded){
	for(int i = 0; i < dim; i++){
		expanded[i] = 3*centroid[i]-2*highest[i];
	}
}

void contraction(int dim, double * highest, double * centroid, double * contracted){
	for(int i = 0; i < dim; i++){
		contracted[i] = 0.5*centroid[i]+0.5*highest[i];
	}
}

void reduction(int dim, double simplex[][dim], int lo){
	for(int k = 0; k < dim + 1; k++){
		if(k != lo){
			for(int i = 0; i < dim; i++){
				simplex[k][i] = 0.5*(simplex[k][i]+simplex[lo][i]);
			}
		}
	}
}

double distance(int dim, double * a, double * b){
	double s = 0;
	for(int i = 0; i < dim; i++){
		s += pow(a[i]-b[i], 2);
	}
	return sqrt(s);
}

double size(int dim, double simplex[][dim]){
	double s = 0;
	for(int k = 1; k < dim + 1; k++){
		double dist = distance(dim, simplex[0], simplex[k]);
		if(dist > s) s = dist;
	}
	return s;
}
