#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

struct rparams{
    double a;
    double b;
};

//f
int system_f (const gsl_vector * v, void * params, gsl_vector * f){
	const double x = gsl_vector_get (v, 0);
	const double y = gsl_vector_get (v, 1);
	double A = 10000;
	gsl_vector_set (f, 0, A*x*y-1);
	gsl_vector_set (f, 1, exp(-x)+exp(-y)-1-1.0/A);
	return GSL_SUCCESS;
}


int rosenbrock_f (const gsl_vector * v, void *params, gsl_vector * f){
	const double x = gsl_vector_get (v, 0);
	const double y = gsl_vector_get (v, 1);
	gsl_vector_set (f, 0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
	gsl_vector_set (f, 1, 100*2*(y-x*x));
	return GSL_SUCCESS;
}

int himmelb_f (const gsl_vector * v, void *params, gsl_vector * f){
	const double x = gsl_vector_get (v, 0);
	const double y = gsl_vector_get (v, 1);
	gsl_vector_set (f, 0,  2*(2*x*(x*x+y-11)+x+y*y-7));
	gsl_vector_set (f, 1,  2*(x*x+2*y*(x+y*y-7)+y-11));
	return GSL_SUCCESS;
}

//df
int system_df (const gsl_vector * v, void *params, gsl_matrix * J){
	double A = 10000;
	const double x = gsl_vector_get(v, 0);
	const double y = gsl_vector_get(v, 1);
        gsl_matrix_set(J, 0, 0, A*y);
        gsl_matrix_set(J, 0, 1, A*x);
        gsl_matrix_set(J, 1, 0, -exp(-x));
        gsl_matrix_set(J, 1, 1, -exp(-y));
	return GSL_SUCCESS;
}


int rosenbrock_df (const gsl_vector * v, void *params, gsl_matrix * J){
	const double x = gsl_vector_get(v, 0);
	const double y = gsl_vector_get(v, 1);
	gsl_matrix_set (J, 0, 0, 1200*x*x-400*y+2);
	gsl_matrix_set (J, 0, 1, -400*x);
	gsl_matrix_set (J, 1, 0, -400*x);
	gsl_matrix_set (J, 1, 1, 200);
	return GSL_SUCCESS;
}


int himmelb_df (const gsl_vector * v, void *params, gsl_matrix * J){
	const double x = gsl_vector_get(v, 0);
	const double y = gsl_vector_get(v, 1);
	gsl_matrix_set (J, 0, 0, 12*x*x+4*y-42);
	gsl_matrix_set (J, 0, 1,  4*x+4*y);
	gsl_matrix_set(J, 1, 0, 4*x+4*y);
	gsl_matrix_set (J, 1, 1,  12*y*y+4*x-26);
	return GSL_SUCCESS;
}


int system_fdf(const gsl_vector * x, void * params, gsl_vector * f, gsl_matrix * J){
	system_f(x, params, f);
	system_df(x, params, J);
	return GSL_SUCCESS;
}

int rosenbrock_fdf (const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J){
	rosenbrock_f (x, params, f);
	rosenbrock_df (x, params, J);
	return GSL_SUCCESS;
}


int himmelb_fdf (const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J){
	himmelb_f (x, params, f);
	himmelb_df (x, params, J);
	return GSL_SUCCESS;
}


int print_state_f(size_t iter, gsl_multiroot_fsolver * s){
	printf ("iter = %3lu; x = % .3f % .3f; f(x) = % .3e % .3e;\n",
        	iter,
        	gsl_vector_get (s->x, 0),
		gsl_vector_get (s->x, 1),
		gsl_vector_get (s->f, 0),
		gsl_vector_get (s->f, 1));
}


int print_state_fdf(size_t iter, gsl_multiroot_fdfsolver * s){
	printf ("iter = %3lu; x = % .3f % .3f; f(x) = % .3e % .3e;\n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->f, 0),
          gsl_vector_get (s->f, 1));
}

//the gsl solver
int gsl_system(double x0_init, double x1_init){
	const gsl_multiroot_fdfsolver_type *T;
	gsl_multiroot_fdfsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function_fdf f = {&system_f, &system_df,	&system_fdf, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fdfsolver_gnewton;
	s = gsl_multiroot_fdfsolver_alloc (T, n);
	gsl_multiroot_fdfsolver_set(s, &f, x);
	printf("Start values (with Jacobian):\n");
	print_state_fdf(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fdfsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (with Jacobian):\n");
	print_state_fdf(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fdfsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}

int gsl_system_no_jacobian(double x0_init, double x1_init){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function f = {&system_f, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fsolver_dnewton;
	s = gsl_multiroot_fsolver_alloc (T, n);
	gsl_multiroot_fsolver_set(s, &f, x);
	printf("Start values (without Jacobian):\n");
	print_state_f(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (without Jacobian):\n");
	print_state_f(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}



int gsl_rosenbrock(double x0_init, double x1_init){
	const gsl_multiroot_fdfsolver_type *T;
	gsl_multiroot_fdfsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function_fdf f = {&rosenbrock_f, &rosenbrock_df,	&rosenbrock_fdf, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fdfsolver_gnewton;
	s = gsl_multiroot_fdfsolver_alloc (T, n);
	gsl_multiroot_fdfsolver_set(s, &f, x);
	printf("Start values (with Jacobian):\n");
	print_state_fdf(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fdfsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (with Jacobian):\n");
	print_state_fdf(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fdfsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}

int gsl_rosenbrock_no_jacobian(double x0_init, double x1_init){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function f = {&rosenbrock_f, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fsolver_dnewton;
	s = gsl_multiroot_fsolver_alloc (T, n);
	gsl_multiroot_fsolver_set(s, &f, x);
	printf("Start values (without Jacobian):\n");
	print_state_f(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (without Jacobian):\n");
	print_state_f(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}


int gsl_himmelb(double x0_init, double x1_init){
	const gsl_multiroot_fdfsolver_type *T;
	gsl_multiroot_fdfsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function_fdf f = {&himmelb_f, &himmelb_df, &himmelb_fdf, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fdfsolver_gnewton;
	s = gsl_multiroot_fdfsolver_alloc (T, n);
	gsl_multiroot_fdfsolver_set(s, &f, x);
	printf("Start values (with Jacobian):\n");
	print_state_fdf(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fdfsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (with Jacobian):\n");
	print_state_fdf(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fdfsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}

int gsl_himmelb_no_jacobian(double x0_init, double x1_init){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	int status;
	size_t iter = 0;
	const size_t n = 2;
	struct rparams p = {1, 1};						//not used
	gsl_multiroot_function f = {&himmelb_f, n, &p};
	gsl_vector * x = gsl_vector_alloc (n);
	//setting initial x values
	gsl_vector_set(x, 0, x0_init);
	gsl_vector_set(x, 1, x1_init);
	T = gsl_multiroot_fsolver_dnewton;
	s = gsl_multiroot_fsolver_alloc (T, n);
	gsl_multiroot_fsolver_set(s, &f, x);
	printf("Start values (without Jacobian):\n");
	print_state_f(iter, s);
	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate (s);
		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-6);
	}
  	while (status == GSL_CONTINUE && iter < 10000);
	printf("Final values (without Jacobian):\n");
	print_state_f(iter, s);
  	//printf ("status = %s\n", gsl_strerror (status));
  	gsl_multiroot_fsolver_free(s);
  	gsl_vector_free (x);
  	return 0;
}
