#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>

struct rparams{
        double a;
        double b;
        };

int rosenbrock_f (const gsl_vector * v, void * params, gsl_vector * f){
        double a = ((struct rparams *) params) -> a;
        double b = ((struct rparams *) params) -> b;
        /* Meaning: params is originally a pointer with type void which we cast to type struct
        - more specifically a struct pointer to 'rparams'. We then use this new pointer
        together with '->' to access variable a or b!*/

        const double x = gsl_vector_get(v, 0);
        const double y = gsl_vector_get(v, 1);
        /* Here we create a pointer 'v' to a gsl_vector which is a struct and contains
        x and y. */

        const double f_1 = a*(1-x);
        const double f_2 = b*(y-x*x);
        gsl_vector_set(f, 0, f_1);
        gsl_vector_set(f, 1, f_2);
        return GSL_SUCCESS;
}


int print_state(size_t iter, gsl_multiroot_fsolver * s){
	printf("iter = %3lu x = % .3f % .3f" "f(x) = % .3e % .3e\n",
	iter,
	gsl_vector_get(s->x, 0),
	gsl_vector_get(s->x, 1),
	gsl_vector_get(s->f, 0),
	gsl_vector_get(s->f, 1));
	return 0;
}
/* s is a pointer to the workspace of the solver. We pull out the first and second
element of the vectors x and f and print them.*/


int main(void){

	const gsl_multiroot_fsolver_type * T;
	gsl_multiroot_fsolver * s;


/*	struct s_v_out svout = oursolver();
	s = svout.s;
	x = svout.v;
*/

	int status;
	size_t i, iter = 0;

	const size_t n = 2;
	struct rparams p = {1.0, 10.0};
	gsl_multiroot_function f = {&rosenbrock_f, n, &p};

	double x_init[2] = {-10.0, -5.0};
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector_set(x, 0, x_init[0]);
	gsl_vector_set(x, 1, x_init[1]);

	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc(T, 2);
	gsl_multiroot_fsolver_set(s, &f, x);

	print_state(iter, s);

	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);
		print_state(iter,s);

		if(status) {break;}
		status = gsl_multiroot_test_residual(s->f, 1e-7);
	} while (status == GSL_CONTINUE && iter < 1000);

	printf("status = %s\n", gsl_strerror(status));

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	return 0;
	/* 'iterate' performs a single iteration of a solver.
	'test_residual' test if the sum of the numerical value of the functions is less
	than epsabs - if it is it returns GLS_SUCCESS if not GLS_CONTINUE. We pull
	f from s!
	This is done while iter is less than 1000.
	Finally we print the error code - gsl_strerror. We end by freein the mememory.*/
}



struct s_v_out{
        gsl_multiroot_fsolver * s;
        gsl_vector * v;
        };


struct s_v_out oursolver (){
        const gsl_multiroot_fsolver_type * T;
        gsl_multiroot_fsolver * s;

        const size_t n = 2;
        struct rparams p = {1.0, 10.0};
        gsl_multiroot_function f = {&rosenbrock_f, n, &p};
        /*size_t is a type: unsigned integer.
        'function' takes three parameters - the function (int pointer), size of the system
        and a pointer to the actual parameters.*/

        double x_init[2] = {-10.0, -5.0};
        gsl_vector* v = gsl_vector_alloc(n);
        gsl_vector_set(v, 0, x_init[0]);
        gsl_vector_set(v, 1, x_init[1]);
        /* Remember that x_init has a particular form!
        */

        T = gsl_multiroot_fsolver_hybrids;
        s = gsl_multiroot_fsolver_alloc(T,2);
        gsl_multiroot_fsolver_set(s, &f, v);
        struct s_v_out svout = {s,v};
        /*We have defined T and s with their types now we specify their values!
        'hybrids' is just a setting of our solver system.
        'alloc' returns a pointer to a n=2 dim solver system with 'hybrids' setting.
        Lastly, we specify the function we want to solve.*/
        return svout;
}
