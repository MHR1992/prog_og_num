#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define EBS (1.0/524288)

//setting grad elements to (beta(xi+dxi)-beta(xi))/dxi
void numeric_gradient(double beta(gsl_vector*), gsl_vector * x, gsl_vector * grad){
	double fx = beta(x);
	for(int i=0; i< x->size; i++){
		double xi = gsl_vector_get(x,i);
		double dx = fabs(xi)*EBS;
		if(fabs(xi) < sqrt(EBS)) dx = EBS;
		gsl_vector_set(x, i, xi + dx);
		gsl_vector_set(grad, i, (beta(x)-fx)/dx);
		gsl_vector_set(x, i, xi);
	}
}


//in the below Dx is the step (also contains lambda), gradient is gradient at x
int qnewton(double beta(gsl_vector*), gsl_vector * x, double acc) {
	int n = x->size, nsteps = 0, nbad = 0, ngood = 0;

	gsl_matrix * B = gsl_matrix_alloc(n, n);
	gsl_vector * gx = gsl_vector_alloc(n);
	gsl_vector * Dx = gsl_vector_alloc(n);
	gsl_vector * z = gsl_vector_alloc(n);
	gsl_vector * gz = gsl_vector_alloc(n);
	gsl_vector * y = gsl_vector_alloc(n);
	gsl_vector * u = gsl_vector_alloc(n);

	gsl_matrix_set_identity(B);					//inverse Hessian matrix
	numeric_gradient(beta, x, gx);
	double fx = beta(x), fz;
	while(nsteps < 1000){
		nsteps++;
		if(fx < acc){
			fprintf(stderr, "qnewton: |Dx| < EPS*|x|\n");
			break;
		}
		gsl_blas_dgemv(CblasNoTrans, -1, B, gx, 0, Dx);		//Dx = skridt = -B*gradient
		if(gsl_blas_dnrm2(gx) < acc){
			fprintf(stderr, "qnewton: |grad| < acc\n");
			break;
		}
		double lambda = 1;
		while(1){						//backtracking
			gsl_vector_memcpy(z, x);
			gsl_vector_add(z, Dx);				//z=x+Dx
			fz = beta(z);					//fz = beta(x+Dx)
			double sTg; gsl_blas_ddot(Dx, gx, &sTg);	//gradient_T (-B gradient)
			if(fz < fx + 0.01*sTg){
				ngood++;
				break;
			}
			if(lambda < 1.0/32){
				nbad++;
				break;
			}
			lambda *= 0.5;
			gsl_vector_scale(Dx, 0.5);
		}

		numeric_gradient(beta, z, gz);			// gz = (beta(x+Dx+dx)-beta(x+Dx))/dx
		gsl_vector_memcpy(y, gz);			// y = gz
		gsl_blas_daxpy(-1, gx, y); 			// y = y-gradient: see just below (12)
		gsl_vector_memcpy(u, Dx); 			// remember s = Dx, so u = s now
		gsl_blas_dgemv(CblasNoTrans, -1, B, y, 1, u); 	// u = 1*u-1*By = s-By
		double sTy, uTy;				// preparing for condition below (14) (see also 16)
		gsl_blas_ddot(Dx, y, &sTy);
		if(fabs(sTy) > 1e-12){
			gsl_blas_ddot(u, y, &uTy);
			double gamma = uTy/2/sTy;		// (16)
			gsl_blas_daxpy(-gamma, Dx, u); 		// u=u-gamma*s
			gsl_blas_dger(1.0/sTy, u, Dx, B);	// sym Broyden update
			gsl_blas_dger(1.0/sTy, Dx, u, B);	// sym Broyden update
		}
		gsl_vector_memcpy(x, z);
		gsl_vector_memcpy(gx, gz);
		fx = fz;
	}
	gsl_matrix_free(B);
	gsl_vector_free(gx);
	gsl_vector_free(Dx);
	gsl_vector_free(z);
	gsl_vector_free(gz);
	gsl_vector_free(y);
	gsl_vector_free(u);

	fprintf(stderr, "qnewton: nsteps=%i ngood=%i nbad=%i fx=%.1e\n", nsteps, ngood, nbad, fx);
	return nsteps;
}




