#ifndef HAVE_QSPLINE_H
#define HAVE_QSPLINE_H

typedef struct {int n; double * x, * y, *b, * c;} qspline; //again we make the struct into a "type"
qspline * qspline_alloc(int n, double * x, double * y);
double qspline_evaluate(qspline * s, double z);
double qspline_derivative(qspline * s, double z);
double qspline_integral(qspline * s, double z);
void qspline_free(qspline * s);

#endif

/*' ifndef: HAVE_QSPLINE_H' ifndef defines the token HAVE_QSPLINE_H if it is not previously defines
HAVE_QSPLINE is defined by the code between it and endif.*/
