#include<math.h>
#include<assert.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_integration.h>

void plainmc(int dim, int N, gsl_vector * a, gsl_vector * b, double f(gsl_vector * x),
        double * result, double * err);

//void randomx(int dim, gsl_vector * a, gsl_vector * b, gsl_vector * x);


int main(){

        double sqrt_of_mod(gsl_vector * x){
		double sum = 0, x_i, x_i_sqrd;
		for(int i = 0; i < x->size; i++){
			x_i = gsl_vector_get(x, i);
			x_i_sqrd = x_i*x_i;
			sum += x_i_sqrd;
                }
		double size = sqrt(sum);
		return sqrt(size);
        }

	double dome(gsl_vector * v){
		double r = gsl_vector_get(v, 0);
		double phi = gsl_vector_get(v, 2);
		return r*r*sin(phi);
	}



	double sing_integ(gsl_vector * x){
		assert(x->size == 3);
		double product = 1;
		for(int i = 0; i < x->size; i++){
			product *= cos(gsl_vector_get(x,i)); 
		}
		return 1/(1-product)/(M_PI*M_PI*M_PI);
	}

	printf("\nEXERCISE A - Plain Monte Carlo integration:\n");
	int dim = 3, N = 10000;
	double result, err;
	gsl_vector * a = gsl_vector_alloc(dim);
	gsl_vector * b = gsl_vector_alloc(dim);
	gsl_vector_set_all(a, 0);
	gsl_vector_set_all(b, 1);
	plainmc(dim, N, a, b, sqrt_of_mod, &result, &err);
	printf("\nFor function sqrt(mod(x)):\n\n");
	printf("Result:\t\t\t%g\nErr:\t\t\t%g\n", result, err);

	gsl_vector_set_all(a, 0);
	gsl_vector_set(b, 0, 1); gsl_vector_set(b, 1, 2*M_PI); gsl_vector_set(b, 2, M_PI/2);
	plainmc(dim, N, a, b, dome, &result, &err);
	printf("\nIntegral of spherical surface:\n\n");
	printf("Volume of sphere:\t%g\nErr:\t\t\t%g\n", 2*result, 2*err);
	printf("Volume should be:\t%g\n", (4.0/3)*M_PI);

	gsl_vector_set_all(b, M_PI);
	plainmc(dim, N, a, b, sing_integ, &result, &err);
	printf("\nThe difficult singular integral:\n\n");
	printf("Result:\t\t\t%g\nErr:\t\t\t%g\n", result, err);
	printf("Result should be:\t%g\n", 1.3932);
	printf("We see that the Monte Carlo integration mangages to calculate the singular integral!");


	//Exercise B - checking that error behaves as O(1/sqrt(N))
	printf("\n\n\n");
	printf("EXERCISE B\n\n");
	printf("From 'plot.svg' we see that the error of the Monte-Carlo method behaves\nas O(1/sqrt(N). The data for the plot is listed below.\n");
	dim = 3;
	gsl_vector_set_all(a, 0);
	gsl_vector_set(b, 0, 1); gsl_vector_set(b, 1, 2*M_PI); gsl_vector_set(b, 2, M_PI/2);
	printf("#N \t #err\n");
	for(int N = 10; N < 5010; N += 10){
		plainmc(dim, N, a, b, dome, &result, &err);
		printf("%d\t %g\n", N, err);

	}
return 0;
}
