#include<stdio.h>
#include<math.h>
#include"mv_operations.h"
#include"qr_gs.h"
#define EPS 1.0/1048576

int newton(double f(vector * x, vector * df, matrix * H), vector * x, double acc){
	int n = x->size, nsteps = 0, nbad = 0, ngood = 0;

	vector * Dx = vector_alloc(n);
	vector * z = vector_alloc(n);
	vector * dfx = vector_alloc(n);
	matrix * Hx = matrix_alloc(n, n);
	vector * dfz = vector_alloc(n);
	matrix * Hz = matrix_alloc(n, n);
	matrix * R = matrix_alloc(n, n);					//for QR decomposition
	double fx;

	while(nsteps < 1000){
		nsteps++;							//Dx = skridt
		fx = f(x, dfx, Hx);
		double fz;
		qr_gs_decomp(Hx, R);				//solve (5) by QR decomposition
		qr_gs_solve(Dx, Hx, R, dfx);			//dfx is then b, Dx is x in (7) from linear equations
		vector_scale(Dx, -1);
		if(vector_modulus(Dx) < EPS * vector_modulus(x)){
			fprintf(stderr, "qnewton: |Dx| < EPS*|x|\n"); break;
		}
		if(vector_modulus(dfx) < acc){
			fprintf(stderr, "qnewton: |grad| < acc\n"); break;
		}
		double lambda = 1;
		while(1){
			vector_cpy(z, x);
			vector_add(1, z, 1, Dx);				//z = x+Dx
			fz = f(z, dfz, Hz);						//fz = beta(x+Dx)
			double sTg = vector_dot(Dx, dfx);			//sTg = gradient_T (-B gradient)

			if(fz < fx + 0.01 * sTg){
				ngood++;
				break;
			}
			if(lambda < EPS){		//changed from EPS
				nbad++;
				break;
			}
			lambda *= 0.5;
			vector_scale(Dx,  0.5);
		}
		vector_cpy(x, z);
		vector_cpy(dfx, dfz);
		matrix_cpy2(Hx, Hz);
		fx = fz;
	}
	vector_free(Dx);
	vector_free(z);
	vector_free(dfx);
	matrix_free(Hx);
	vector_free(dfz);
	matrix_free(Hz);
	matrix_free(R);

	fprintf(stderr, "qnewton: nsteps = %i ngood%i nbad = %i fx = %.1e\n", nsteps, ngood, nbad, fx);
	return nsteps;
}

//setting grad elements to (beta(xi+dxi)-beta(xi))/dxi
void numeric_gradient(double beta(vector *), vector * x, vector * grad){
	double fx = beta(x);
	for(int i = 0; i < x->size; i++){
		double xi = vector_get(x, i);
		double dx = fabs(xi)*EPS;
		if(fabs(xi) < sqrt(EPS)) dx = EPS;
		vector_set(x, i , xi+dx);
		vector_set(grad, i, (beta(x)-fx)/dx);
		vector_set(x, i, xi);
	}
}
//in the below Dx is the step (also contains lambda), gradient is gradient at x 
int qnewton(double beta(vector *), vector * x, double acc){
	int n = x->size, nsteps = 0, nbad = 0, ngood = 0;

	matrix * B = matrix_alloc(n, n);
	vector * gradient = vector_alloc(n);
	vector * Dx = vector_alloc(n);
	vector * z = vector_alloc(n);
	vector * gz = vector_alloc(n);
	vector * y = vector_alloc(n);
	vector * u = vector_alloc(n);

	matrix_set_identity(B);							//inverse hessian matrix
	numeric_gradient(beta, x, gradient);
	double fx = beta(x), fz;
	while(nsteps < 1000){
		nsteps++;							//Dx = skridt
		matrix_vector_dot2(Dx, -1, B, ' ', gradient);			//Dx = -Bgradient
		if(vector_modulus(Dx) < EPS * vector_modulus(x)){
			fprintf(stderr, "qnewton: |Dx| < EPS*|x|\n"); break;
		}
		if(vector_modulus(gradient) < acc){
			fprintf(stderr, "qnewton: |grad| < acc\n"); break;
		}
		double lambda = 1;
		while(1){
			vector_cpy(z, x);
			vector_add(1, z, 1, Dx);				//z = x+Dx
			fz = beta(z);						//fz = beta(x+Dx)
			double sTg = vector_dot(Dx, gradient);			//sTg = gradient_T (-B gradient)
			if(fz < fx + 0.01 * sTg){
				ngood++;
				break;
			}
			if(lambda < EPS){
				nbad++;
				break;
			}
			lambda *= 0.5;
			vector_scale(Dx, 0.5);
		}
		numeric_gradient(beta, z, gz);				// gz = (beta(x+Dx+dx)-beta(x+Dx))/dx
		vector_cpy(y, gz);					// y = gz
		vector_sub(1, y, 1, gradient);				// y = y-gradient: see just below (12)
		vector_cpy(u, Dx);					// remember s = Dx, so u = s now
		matrix_vector_add_dot(u, 1, -1,  B, ' ', y);		// u = 1*u-1*By = s-By
		double sTy = vector_dot(Dx, y);				// preparing for condition below (14)
		if(fabs(sTy) > 1e-12){
			double uTy = vector_dot(u, y);			//(16)
			double gamma = uTy/2/sTy;			//(16)
			vector_sub(1, u, gamma, Dx);			//u = u - gamma*s
			matrix_add_outprod(B, 1.0/sTy, u, Dx);	//symmetric broyden update
			matrix_add_outprod(B, 1.0/sTy, Dx, u);	//(15)
		}
		vector_cpy(x, z);
		vector_cpy(gradient, gz);
		fx = fz;
	}
	matrix_free(B);
	vector_free(gradient);
	vector_free(Dx);
	vector_free(z);
	vector_free(gz);
	vector_free(y);
	vector_free(u);
	fprintf(stderr, "qnewton: nsteps = %i ngood%i nbad = %i fx = %.1e\n", nsteps, ngood, nbad, fx);
	return nsteps;
}
