#include<math.h>
#include<stdio.h>
#include"qr_gs.h"
#include"mv_operations.h"

void lsfit(int m, double f(int i, double x),
        vector * x, vector * y, vector * dy,
        vector * c, matrix * S);


int main(){
	//Initializing data
	double x[] = {0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900};
	double y[] = {12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355};
	double dy[] = {0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002};	//experimental errors
	int n = sizeof(x)/sizeof(x[0]);

	for(int i = 0; i < n; i++) printf("%g %g %g\n",x[i],y[i],dy[i]);
	printf("\n\n");

	vector * v_x = vector_alloc(n);
	vector * v_y = vector_alloc(n);
	vector * v_err_y = vector_alloc(n);
	for(int i = 0; i < n; i++){
		vector_set(v_x,i,x[i]); vector_set(v_y,i,y[i]); vector_set(v_err_y,i,dy[i]);
	}

	//Exercise 1.2
	int m = 3;
	double funs(int i, double x){
   		switch(i){
   			case 0: return 1/x; break;
   			case 1: return 1; break;
   			case 2: return x; break;
   			default: return NAN;
		}
	}

	vector * c = vector_alloc(m);
	matrix * S = matrix_alloc(m, m);						//S is the matrix that is changed to the covariance matrix.
	lsfit(m, funs, v_x, v_y, v_err_y, c, S);						//changing c and S

	vector * err_c = vector_alloc(m);
	for(int k = 0; k < m; k++){
		double skk = matrix_get(S, k, k);
		vector_set(err_c, k, sqrt(skk));						//vector with the errors of the fitting parameters c!
	}

	double fit(double x){
		double sum = 0;
		for(int k = 0; k < m; k++) sum += vector_get(c, k)*funs(k, x);		//sum is F_c(x) from (5)
		return sum;
	}

	double fit_plus(int i, double x){
		return fit(x)+vector_get(err_c, i)*funs(i, x);				//Upper error-function
	}

	double fit_minus(int i, double x){
		return fit(x)-vector_get(err_c, i)*funs(i, x);				//Lower error-function
	}

	double z, dz = (x[n-1]-x[0])/90;						//printing in a smooth curve as number of steps is set to 90
	for(int i = 0; i < m; i++){
		z = x[0]-dz/2;
			do{
				printf("%g %g %g %g\n", z, fit(z), fit_plus(i, z), fit_minus(i, z));
				z += dz;
			}while(z < x[n-1]+dz);
		printf("\n\n");
	}
return 0;

}
