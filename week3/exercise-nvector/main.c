
#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main(){
	int n = 5;

	printf("testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n\n");

	printf("testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (double_equal(vi, value)) printf("test passed\n\n");
	else printf("test failed\n\n");

	nvector * a = nvector_alloc(n);
	nvector * b = nvector_alloc(n);
	nvector * c = nvector_alloc(n);
	double d_prod = 0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
		d_prod += y*y;
	}
	nvector_print("a is: ", a);
	nvector_print("b is: ", b);

	printf("\ntesting nvector_add ...\n");
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);
	if (nvector_equal(c, a)) printf("test passed\n\n");
	else printf("test failed\n\n");
	// Comparing with a as 'add' overwrites the vector a with the sum.

	printf("testing nvector_dot_product ...\n");
	double dot_prod = nvector_dot_product(b, b);
	printf("b dot b should be: %g ", d_prod);
	printf("\nb dot b is actually: %g ", dot_prod);
	if (double_equal(d_prod, dot_prod)) printf("\ntest passed\n\n");
	else printf("test failed\n\n");

	printf("testing nvector_equal ...\n");
	if (nvector_equal(a,a) && !(nvector_equal(a,b))) printf("test passed\n\n");
	else printf("test failed\n\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	return 0;
}

