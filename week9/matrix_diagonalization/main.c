#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"simple_operations.h"
#define RND (double) rand()/RAND_MAX
#define max_print 10

int jacobi(gsl_matrix * A, gsl_vector * e, gsl_matrix * V);

int main(int argc, char** argv){
	int n = (argc > 1? atoi(argv[1]):5);					//"atoi": string to integer
	gsl_matrix * A = gsl_matrix_alloc(n,n);

	for(int r = 0; r < n; r++){
		for(int c = r; c < n; c++){
			double x = RND;
			gsl_matrix_set(A, r, c, x);
			gsl_matrix_set(A, c, r, x);
		}
	}

	gsl_matrix * B = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(B, A); 						//copies A to into B.
	gsl_matrix * V = gsl_matrix_alloc(n,n);
	gsl_vector * e = gsl_vector_alloc(n);
	int sweeps = jacobi(A, e, V);

	if(n < max_print){
		fprintf(stderr, "EXERCISE A:\n\n");
		fprintf(stderr,"Original symmetric matrix A: \n"); matrix_print(B);
		fprintf(stderr,"\nResult of Jacobi diagonalization: \n\n");
		fprintf(stderr, "n = %i, sweeps = %i\n", n, sweeps);
		fprintf(stderr, "eigenvalues:\n"); vector_print(e);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, B, V, 0, A);		//outputs dot(B,V) into A
		gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, V, A, 0, B);		//outputs dot(V,A) into B (becoming (B^T)VB
		fprintf(stderr, "\nCheck that V^TAV==D (same as checking diagonal(B^TVB) == e):\n");
		matrix_print(B);
		fprintf(stderr, "\nWe see that the implementation works as intended!\n");
	}
	//free matrices and vectors!!
	gsl_matrix_free(V);
	gsl_vector_free(e);
	gsl_matrix_free(A);
	gsl_matrix_free(B);

return 0;

}
