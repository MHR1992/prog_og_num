#include"mv_operations.h"
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

//see page 3 below (8) A ends up as Q!
void qr_gs_decomp(matrix * A, matrix * R){				//R is the square matrix
	assert(A->size2 == R->size1 && R->size2 == R->size1);
	int m = A->size2;

	for(int i = 0; i < m; i++){
		vector * a_i = matrix_get_column(A, i);			//vector view is temperary object used to operate on subset of vector elements.
		double R_ii = sqrt(vector_dot(a_i, a_i));					//dnrm2: computes euclidian norm of subvector
		matrix_set(R, i, i, R_ii);
		vector_scale(a_i, 1/R_ii);				//normalization: a_i becomes q_i
		matrix_set_column(A, i, a_i);				//generating matrix Q
		for(int j = i+1; j < m; j++){
			vector * a_j = matrix_get_column(A, j);
			double R_ij = vector_dot(a_i, a_j);		//s is R_ij
			matrix_set(R, i, j, R_ij);			//setting value
			matrix_set(R, j, i, 0);
			vector_add(1,a_j, -R_ij, a_i);			//updating a_j, rule: x = ax+by
			matrix_set_column(A, j, a_j);
			vector_free(a_j);
		}
		vector_free(a_i);
	}
}

void qrbacksub(matrix * R, vector * x){
	int m = R->size1;
	for(int i = m-1; i >= 0; i--){
		double sum = 0;
		for(int k = i+1; k < m; k++){
			sum += matrix_get(R, i, k)*vector_get(x, k);		//sum = dot(R_i,x) for row i
		}
		vector_set(x, i, (vector_get(x,i)-sum)/matrix_get(R, i, i));	//x_i = (x_i - sum)/R_ii
	}
}

//vector x is the one you want to find!
void qr_gs_solve(vector * x, matrix * Q, matrix * R, vector * b){
	//assert(x->size == b->size);
	matrix_vector_dot(x, Q, 'T', b);
	qrbacksub(R, x);
}




void qrinv(matrix * Q, matrix * R, matrix * B){
	int n = R->size1;
	vector * b = vector_alloc(n);
	for(int i = 0; i < n; i++){
		vector_set(b, i, 1.0);
		vector * x = vector_alloc(Q->size2);
		qr_gs_solve(x, Q, R, b);
		vector_set(b, i, 0.0);
		matrix_set_column(B, i, x);
		vector_free(x);
	}
	vector_free(b);
}

