#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main(int argc, char** argv){
	for(int i=1; i < argc; i++) {
		double x = atof(argv[i]);
		printf("%lg \t %lg\n", x, sin(x));
	}
return 0;
}

/* int argc and char** argv are used to send arguments to a program.
argc is the number of arguments sent to the program and the second is a
pointer to a vector with the arguments.*/
