#include<stdio.h>
#include<math.h>
#include<limits.h>

int exercise2() {

	printf("\nExercise 2i:\n");
	int max = INT_MAX/3;
	int i = 1;
	float sum_up_float;
	for(i; i <= max; i++){
	sum_up_float += 1.0f/i;
	}
	float sum_down_float;
	for(i; i >= 1; i--){
	sum_down_float += 1.0f/i;
	}
	printf("sum_up_float is: %g\n", sum_up_float);
	printf("sum_down_float is: %g\n", sum_down_float);

	printf("\n\nExercise 2ii:\n");
	printf("Addition of numbers that are close in magnitude are more precise than adding those that are not. Therefore sum_down_float should be more precise.");

	printf("\n\nExercise 2iii:\n");
	printf("It is a harmonic series that diverges.");

	printf("\n\nExercise 2iiii:\n");
	double sum_up_double;
	double sum_down_double;
	int k = 1;
	for(k; k<=max; k++){
	sum_up_double += 1.0d/k;
	}
	for(k; k>=1; k--){
	sum_down_double += 1.0d/k;
	}

	printf("sum_up_double is: %g\n", sum_up_double);
	printf("sum_down_double is: %g\n", sum_down_double);
	printf("We loose less digits as double is more precise. Hence the numbers are closer.\n");

return 0;
}
