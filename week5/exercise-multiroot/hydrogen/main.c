#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#define FSOLVER gsl_multiroot_fsolver_broyden		//algorithm type is broyden (needs to start close to root)

double F_eps(double eps, double r);

int master(const gsl_vector * x, void * params, gsl_vector * f){
	double eps = gsl_vector_get(x, 0);
	assert(eps < 0);
	double rmax = *(double *) params;
	double f_val = F_eps(eps, rmax);
	gsl_vector_set(f, 0, f_val);
	return GSL_SUCCESS;
}

int print_state(size_t iter, gsl_multiroot_fsolver * s){
        fprintf(stderr, "iter = %3lu x = % .3f f(x) = % .3e\n",
        iter,
        gsl_vector_get(s->x, 0),
        gsl_vector_get(s->f, 0));
        return 0;
}

int main(int argc, char** argv){
	double rmax = argc > 1? atof(argv[1]):10;
	fprintf(stderr, "rmax = %g\n", rmax);

	int dimension = 1;
	gsl_multiroot_fsolver * s;
	s = gsl_multiroot_fsolver_alloc(FSOLVER, dimension);

	gsl_multiroot_function F = {.f = master, .n = dimension, .params = (void *)&rmax};
	gsl_vector * x = gsl_vector_alloc(dimension);
	gsl_vector_set(x, 0, -1);
	gsl_multiroot_fsolver_set(s, &F, x);

	int status, iter = 0;
	const double epsabs = 1e-3;

	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);
		if(status) {break;}

		status = gsl_multiroot_test_residual(s->f, epsabs);
		if(status == GSL_SUCCESS) fprintf(stderr, "converged\n");

		print_state(iter, s);

	} while (status == GSL_CONTINUE && iter < 1000);

	double e = gsl_vector_get(s->x, 0);
	printf("# rmax, e\n");
	printf("%g %g\n", rmax, e);
	printf("\n\n");

	printf("#r #F_eps(e, r) #exact\n");
	for(double r = 0; r <= rmax; r += rmax/64){
		printf("%6.4g %6.4g %6.4g\n", r, F_eps(e, r), r*exp(-r)*F_eps(e, 1)*exp(1));
	}
	for(double r = 0; r <= rmax; r += rmax/64){
		printf("%6.4g %6.4g %6.4g\n", r, F_eps(e, r), r*exp(-r));
	}

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	return EXIT_SUCCESS;
}
