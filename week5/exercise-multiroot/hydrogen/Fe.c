#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#define STEPPER gsl_odeiv2_step_rkf45
#define STARTSTEP 1e-3

//ode system of the SE for hydrogen
int ode_H_s_wave(double r, const double y[], double y_prime[], void * params){
	double e = *(double *) params;
	y_prime[0] = y[1];
	y_prime[1] = 2*(-1/r-e)*y[0];
	return GSL_SUCCESS;
}

double F_eps(double e, double r){
	assert(r >= 0);
	const double rmin = 1e-3;
	if(r < rmin) return r-r*r;

	gsl_odeiv2_system system;
	system.function = ode_H_s_wave;
	system.jacobian = NULL;			//using broyden no jacobian is required
	system.dimension = 2;
	system.params = (void *) &e;

	gsl_odeiv2_driver * driver;
	driver = gsl_odeiv2_driver_alloc_y_new(&system, STEPPER, STARTSTEP, 1e-6, 1e-6);

	double t = rmin, y[] = {t-t*t, 1-2*t};
	int status = gsl_odeiv2_driver_apply(driver, &t, r, y);
	if(status != GSL_SUCCESS){
		 fprintf(stderr, "Fe: odeiv2 error: %d\n", status);
	}

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
