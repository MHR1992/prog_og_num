#include"mv_operations.h"
#include"qr_gs.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#define RND (double) rand()/RAND_MAX

int main(){
	//Exercise A.1
	printf("SOLVING LIN EQUATIONS USING QR DECOMPOSITION AND GRAM-SCHMIDT ORTH\n\n");
	printf("\nExercise A.1:\n\n");
	int n = 5, m = 4;
	matrix * A = matrix_alloc(n,m);
	matrix * R = matrix_alloc(m,m);
	matrix_random(A);
	printf("QR DECOMPOSITION: \n\n");
	printf("A random matrix A:\n"); matrix_print(A);
	matrix * Q = matrix_cpy(A);
	qr_gs_decomp(Q, R);
	printf("Matrix Q:\n");
	matrix_print(Q);
	printf("Matrix R should be upper triangular:\n");
	matrix_print(R);
	matrix * QR = matrix_alloc(Q->size1, R->size2);
	matrix_mm(QR, Q, R);
	printf("Matrix QR should be equal to A:\n");
	matrix_print(QR);
	printf("\nWe see that the QR factorization implementation works.\n");

	//Exercise A.2
	printf("\nExercise A.2:\n\n");
	m = 3;
	printf("Linear system: \n");
	matrix * A2 = matrix_alloc(m, m);
	matrix_random(A2);
	printf("Random matrix A2:\n");
	matrix_print(A2);
	matrix * Q2 = matrix_cpy(A2);
	vector * b = vector_alloc(m);
	vector_random(b);
	printf("Vector b is:\n");
	vector_print(b);
	matrix * R2 = matrix_alloc(m, m);
	qr_gs_decomp(Q2, R2);
	printf("Matrix Q2 is:\n");
	matrix_print(Q2);
	printf("Matrix R2 is,\n");
	matrix_print(R2);
	vector * x = vector_alloc(Q2->size2);
	qr_gs_solve(x, Q2, R2, b);
	printf("solution x (found from in-place backsubstitution) is:\n");
	vector_print(x);
	printf("Ax is (should be b):\n");
	vector * Ax = vector_alloc(A2->size1);
	matrix_vector_dot(Ax, A2, ' ', x);
	vector_print(Ax);
	printf("Backsubstitution implementation works as intended!");

	//Exercise B
	printf("\nExercise B:\n\n");
	matrix * A_inv = matrix_alloc(m, m);				//B = A_inv
	qrinv(Q2, R2, A_inv);
	printf("Inverse of Matrix A2 is:\n");
	matrix_print(A_inv);
	matrix * A_invA = matrix_alloc(A_inv->size1, A2->size2);
	matrix_mm(A_invA, A_inv, A2);
	printf("A^(-1)A is (should be identity matrix):\n");
	matrix_print(A_invA);
	printf("We see that the inverse function works as intended!\n");


	//free
	//Exercise 1.1
	matrix_free(A);
	matrix_free(R);
	matrix_free(Q);
	matrix_free(QR);
	//Exercise 1.2
	matrix_free(A2);
	matrix_free(Q2);
	vector_free(b);
	vector_free(x);
	vector_free(Ax);
	//Exercise 2
	matrix_free(A_inv);
	matrix_free(A_invA);
}

