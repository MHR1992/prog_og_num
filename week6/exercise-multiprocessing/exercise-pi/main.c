#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<unistd.h>
#include<math.h>

struct params {int n, N_in; unsigned int seed;};

void * darts_in_counter(void* params){
	struct params * p = (struct params *) params;
	int n = p->n;
	int N_in = 0;
	for(int i = 0; i < n; i++){
		double x = (double) rand_r(&(p->seed))/RAND_MAX;
		double y = (double) rand_r(&(p->seed))/RAND_MAX;
		if(x*x + y*y < 1) N_in++;
	}
	p->N_in = N_in;
	return NULL;
}

double getpi(int n){

	struct params p1, p2, p3;
	//split n in 3 (split load in 3)
	p1.n = n/3; p1.seed = 3;
	p2.n = n/3; p2.seed = 4;
	p3.n = n/3; p3.seed = 5;

	//creating three threads
	pthread_t t1,t2,t3;

	//parallel computing (the 2nd arg points to special attributes of the thread)
	pthread_create(&t1, NULL, darts_in_counter, (void *) &p1);
	pthread_create(&t2, NULL, darts_in_counter, (void *) &p2);
	pthread_create(&t3, NULL, darts_in_counter, (void *) &p3);

	//pthread_join "joins the threads" (waits for specified thread to terminate)
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	pthread_join(t3, NULL);
	//pi = 4*N_in/N
	double pi = 4*(double) (p1.N_in + p2.N_in + p3.N_in) / (p1.n + p2.n + p3.n);
	return pi;
	}

int main() {

	for(int n = 10000; n < 1e6; n += 10000){
		double pi_n = getpi(n);
		printf("%g %g\n", (double)n , fabs(pi_n-M_PI));
	}
return 0;
}
